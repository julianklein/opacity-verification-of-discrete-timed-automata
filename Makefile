CC = gcc

LDFLAGS +=-lstdc++ -std=c++17 -lpthread -lm -ldl
CFLAGS +=-g -O0 -Wall
CFLAGS +=-I source/json
CFLAGS +=-I source/utils
CFLAGS +=-I source/synthesis
CFLAGS +=-I source/analysis
CFLAGS +=-I source/automata
CFLAGS +=-I source/automata/fa
CFLAGS +=-I source/automata/ta

CPPFILES +=./source/json/jsoncpp.cpp
CPPFILES +=./source/utils/utils.cpp
CPPFILES +=./source/synthesis/synthesis.cpp
CPPFILES +=./source/automata/automata_reader.cpp
CPPFILES +=./source/analysis/get_fa_from_ta.cpp
CPPFILES +=./source/analysis/misc_functions.cpp
CPPFILES +=./source/analysis/mask_ta.cpp
CPPFILES +=./source/analysis/determinization_fa.cpp

OBJECTS =$(addprefix build/,$(CPPFILES:%.cpp=%.o))

EVALUATORCPPFILES +=./evaluation/evaluation.cpp

UNITTESTOBJECTS =$(addprefix build/,$(UNITTESTCPPFILES:%.cpp=%.o))
EVALUATOROBJECTS =$(addprefix build/,$(EVALUATORCPPFILES:%.cpp=%.o))

DEPS := $(OBJECTS:.o=.d) $(UNITTESTOBJECTS:.o=.d) $(EQTESTOBJECTS:.o=.d) $(EVALUATOROBJECTS:.o=.d)
-include $(DEPS)

clean:
	rm -rf ./build

build/%.o : %.cpp
	mkdir -p $(dir $@) 
	$(CC) -c $(CFLAGS) -MMD $< -o $@ $(LDFLAGS)

print: $(OBJECTS)
	$(CC) ./source/main.cpp $^ $(CFLAGS) -o printer $(LDFLAGS)

evaluator: $(OBJECTS) $(EVALUATOROBJECTS) 
	$(CC) $^ $(CFLAGS) -o evaluator $(LDFLAGS)

.PHONY: clean