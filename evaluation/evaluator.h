#ifndef EVALUATOR_H_
#define EVALUATOR_H_

#include <iostream>
#include <list>
#include <tuple>
#include <thread>
#include <mutex>
#include <chrono>
#include <vector>
#include <functional>
#include <map>
#include <sstream>
#include <iomanip>
#include <fstream>
#include <filesystem>
#include <cmath>

#include "constants.h"

/*
Contains the class Evaluator, representing the basic format for an evaluation process. The class provides an interface
to specify an evaluation process process with great detail with the constructor. A user can provide the number of
runs with a number of threads to be used on multiple test levels for a given test mode in a provided test function. All
results are automatically gathered, averaged, and stored to a csv-file.
The provided test function takes a mode and a level as input for testing. The level refers to the value of a
parameter (for example the number of states or symbols of a TA). The mode then specifies, in which way the level
is interpreted (for example mode = states could mean level = number of states). The evaluator executes each
level in the specified mod using the test functions a given number of times, which is specified with the number of runs.
To accelerate this process, multiple threads can be used (specified by the number of threads). The test function is
expected to provide a result in the correct format.
*/

class Evaluator
{
private:
    // name of evaluator
    std::string name_; 

    // member variables with mutex
    std::mutex m_available_runs_;
    int available_runs_;
    
    std::mutex m_reported_runs_;
    int reported_runs_;

    std::mutex m_result_;
    std::vector<double> result_;

    std::mutex m_print_;

    // evaluation configuration
    int number_threads_; // number of threads to be used
    int number_levels_;  // number of levels (int variable for test_function)
    int number_runs_;    // number of runs to be completed on each level
    
    // mode that is passed to test_function_
    std::string mode_; // string variable of test_function
    // function that tests and provides results
    std::function<std::vector<double>(std::string, int)> test_function_;

    // result parameters
    std::vector<std::string> result_names_;
    unsigned int result_length_;
    
    // files
    std::string path_;
    std::string out_file_;

public:
    // constructors
    Evaluator(
        std::string name,
        std::string mode,
        int number_threads,
        int number_levels,
        int number_runs,
        std::function<std::vector<double>(std::string, int)> test_function,
        std::vector<std::string> result_names
    )
    {
        name_ = name;
        mode_ = mode;
        test_function_ = test_function;
        number_threads_ = number_threads;
        number_levels_ = number_levels;
        number_runs_ = number_runs;
        available_runs_ = number_runs;
        reported_runs_ = 0;
        result_names_ = result_names;
        result_length_ = result_names_.size();
        result_ = std::vector<double>(result_length_, 0);
        path_ = name_;
    }

    // thread calls this function to get the next job
    int ask_for_job(const unsigned int thread_id)
    {
        // lock mutex to look for new jobs
        m_available_runs_.lock();
        // result stores job id (-1 means no new job available)
        int result = -1;
        if (available_runs_ > 0)
        {
            result = number_runs_ - available_runs_;
            available_runs_ = available_runs_ - 1;
        }
        else
        {
            result = -1;
        }
        // unlock mutex
        m_available_runs_.unlock();
        return result;
    }

    // thread calls this function after completing a job
    void report_job(const unsigned int thread_id, const unsigned int job_id)
    {
        // lock mutex for reported runs
        m_reported_runs_.lock();
        // calculate percentage of completed runs befor current completion
        double old_percentage = 100 * double(reported_runs_) / double(number_runs_);
        reported_runs_ = reported_runs_ + 1;
        // calculate new percentage
        double new_percentage = 100 * double(reported_runs_) / double(number_runs_);
        
        // if percentage changed, we update the status bar
        if (std::floor(old_percentage) < std::floor(new_percentage))
        {
            print("#", "magenta");
        }

        // check if we are done
        if (reported_runs_ >= number_runs_)
        {
            print("|\n", "white");
        }
        // unlock mutex of reported runs
        m_reported_runs_.unlock();
    }

    // this function is executed by each thread
    void runEvaluator(const unsigned int thread_id, const int level)
    {
        // get initial job
        int new_job_id = ask_for_job(thread_id);

        // work until no new jobs are available
        while (new_job_id >= 0)
        {
            // compute test results with test_function
            std::vector<double> write_back = test_function_(mode_, level);

            // lock mutex to add result
            m_result_.lock();

            // add result
            for (unsigned int i = 0; i < result_length_; i++)
            {
                result_[i] = result_[i] + write_back[i];
            }

            // unlock mutex
            m_result_.unlock();

            // report job and get next job
            report_job(thread_id, new_job_id);
            new_job_id = ask_for_job(thread_id);
        }
    }

    // main function to run evaluation
    void run()
    {
        // initial print with configuration
        print("Evaluator started with the following configuration:\n");
        print("  Name       : " + name_ + "\n", "yellow");
        print("  Levels     : " + std::to_string(number_levels_) + "\n", "yellow");
        print("  Runs       : " + std::to_string(number_runs_) + "\n", "yellow");
        print("  Threads    : " + std::to_string(number_threads_) + "\n", "yellow");


        // handle file paths
        std::string store_path = std::string(OUTPUT_PATH) + std::string("/evaluation/" + path_);
        if (!std::filesystem::exists(store_path)) 
        {
            if (!std::filesystem::create_directories(store_path)) 
            {
                std::cerr << "Failed to create directory: " << store_path << std::endl;
                exit(0);
            }
        }
        
        out_file_ = std::string(OUTPUT_PATH) + std::string("/evaluation/" + path_ + "/out.csv");
        std::ofstream out;
        out.open(out_file_);
        if (out.is_open()) 
        {
            for (unsigned int i = 0; i < result_length_; i++)
            {
                out << result_names_[i];
                if (i < result_length_ - 1)
                {
                    out << ", ";
                }
            }
            out << "\n";
            out.close();
        } 
        else 
        {
            std::cerr << "Unable to create or write to the file." << std::endl;
            exit(0);
        }

        // main loop starts here
        for(int level = 0; level < number_levels_; level++)
        {
            // initial print for current level
            print("Running experiments for level " + std::to_string(level) + "\n", "white");
            print(" ");
            unsigned int length = 100;
            if (number_runs_ < 100)
            {
                length = number_runs_;
            }
            for (unsigned int i = 0; i < length; i++)
            {
                print("_", "white");
            }
            print(" \n|", "white");
            for (unsigned int i = 0; i < result_length_; i++)
            {
                result_[i] = 0;
            }
            available_runs_ = number_runs_;
            reported_runs_ = 0;


            // start the specified number of threads
            std::vector<std::thread> threads (number_threads_);
            for (int i = 0; i < number_threads_; i++)
            {
                threads[i] = std::thread(&Evaluator::runEvaluator, this, i, level);
            }

            // sync all threads
            for (int i = 0; i < number_threads_; i++)
            {
                threads[i].join();
            }
            // here, we are done


            // store output
            out.open(out_file_, std::ofstream::app);
            if (out.is_open()) 
            {
                for (unsigned int i = 0; i < result_length_; i++)
                {
                    out << std::to_string(result_[i] / (double)(number_runs_));
                    if (i < result_length_ - 1)
                    {
                        out << ", ";
                    }
                }
                out << "\n";
                out.close();
            } 
            else 
            {
                std::cerr << "Unable to create or write to the file." << std::endl;
                exit(0);
            }
        }
        print("All experiments done!\n", "magenta");
    }

    // print function for better readability in console
    void print(const std::string& message, const std::string& color="standard")
    {
        static const std::map<std::string, std::string> colorMap = 
        {
            {"black", "\033[1;30m"},
            {"red", "\033[1;31m"},
            {"green", "\033[1;32m"},
            {"yellow", "\033[1;33m"},
            {"blue", "\033[1;34m"},
            {"magenta", "\033[1;35m"},
            {"cyan", "\033[1;36m"},
            {"white", "\033[1;37m"}
        };
        auto it = colorMap.find(color);
        m_print_.lock();
        if (it != colorMap.end()) 
        {
            std::cout << it->second << message << "\033[0m" << std::flush;;
        } 
        else 
        {
            std::cout << message << std::flush;;
        }
        m_print_.unlock();
    }
};

#endif // EVALUATOR_H_