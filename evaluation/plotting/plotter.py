import os
import sys
import shutil

from functions import color_text
from functions import find_tex_files
from functions import remove_directory
from functions import create_directory
from functions import get_first_line
from functions import longest_common_prefix


def main():

	OPT_OPEN = False

	if ('open' in sys.argv):
		OPT_OPEN = True


	FILE = sys.argv[1]
	TEX = sys.argv[2]
	TEXFILE = ""



	tex_files = find_tex_files("./")
	
	for t in tex_files:
		s1 = t[2:]
		s2 = s1.replace("plot_", "")
		s3 = s1.replace(".tex", "")
		s4 = s2.replace(".tex", "")

		if ((TEX == s1) or (TEX == s2) or (TEX == s3) or (TEX == s4)):
			TEXFILE = s3



	if (not os.path.exists(FILE)):
		print("ERROR: File " + FILE + " does not exist")
		exit(0)

	if (not FILE.endswith('.csv')):
		print("ERROR: File " + FILE + " is not a csv file")
		exit(0)

	if (TEXFILE == ""):
		print("ERROR: Tex " + TEX + " could not be found")
		exit(0)

	# read out data from csv
	sep = get_first_line(FILE)
	metric_list = sep[1:]
	XVALUE = sep[0]

	METRICS = []
	for m in metric_list:
		x = m[3:]
		if (not x in METRICS):
			METRICS.append(x)

	XLABEL = "invalid"

	if (XVALUE == "States"):
		XLABEL = "n_l"
	if (XVALUE == "MaxTimes"):
		XLABEL = "n_t"

	OUTPATH = FILE[:-4] + "/"

	print(color_text("Arguments are valid, running with the following configuration:", "blue"))
	print(color_text("    TEX     : " + str(TEXFILE), "blue"))
	print(color_text("    FILE    : " + str(FILE), "blue"))
	print(color_text("    X       : " + str(XVALUE), "blue"))
	print(color_text("    XLabel  : " + str(XLABEL), "blue"))
	print(color_text("    METRICS : " + str(METRICS), "blue"))
	print(color_text("    OUTPATH : " + str(OUTPATH), "blue"))

	

	# compute strings for command

	metrics_string = "\\newcommand{\\metricArray}{"
	i = 0
	for m in METRICS:
		metrics_string = metrics_string + m
		if (i < len(METRICS) - 1):
			metrics_string = metrics_string + ", "
		i = i + 1
	metrics_string = metrics_string + "}"

	xvalue_string = "\\newcommand{\\xvalue}{" + XVALUE + "}"

	xvalue_string = xvalue_string + "\\newcommand{\\xlabel}{" + XLABEL + "}"

	csv_string = "\\newcommand{\\outfile}{" + FILE + "}"

	plotter_string = "\\input{" + TEXFILE + "}"



	# create out dir
	remove_directory(OUTPATH)
	create_directory(OUTPATH)



	COMMAND = "pdflatex --output-directory=" + OUTPATH + " \"" + metrics_string + " " + xvalue_string + " " + csv_string + " " + plotter_string + "\""

	print("    COMMAND : " + COMMAND)

	os.system(COMMAND)


	if (OPT_OPEN):
		os.system('xdg-open ' + OUTPATH + TEXFILE + ".pdf")



if __name__ == "__main__":
	main()