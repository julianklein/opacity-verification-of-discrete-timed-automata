import os
import sys
import shutil

# print colored text
def color_text(text, color):
    color_map = {
        "black": "\033[1;30m",
        "red": "\033[1;31m",
        "green": "\033[1;32m",
        "yellow": "\033[1;33m",
        "blue": "\033[1;34m",
        "magenta": "\033[1;35m",
        "cyan": "\033[1;36m",
        "white": "\033[1;37m"
    }
    color_code = color_map.get(color, "")
    if color_code:
        return color_code + text + "\033[0m"
    else:
        return text

def list_directories(path):
    try:
        directories = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]
        return directories
    except OSError as e:
        print(f"An error occurred: {e}")
        return []

def find_tex_files(directory):
    tex_files = []
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith('.tex'):
                tex_files.append(os.path.join(root, file))
    return tex_files

def remove_directory(directory_path):
    try:
        shutil.rmtree(directory_path)
        print(f"Directory '{directory_path}' and its contents removed successfully.")
    except Exception as e:
        print(f"An error occurred while removing '{directory_path}': {e}")

def create_directory(directory_path):
    try:
        os.makedirs(directory_path)
        print(f"Directory '{directory_path}' created successfully.")
    except Exception as e:
        print(f"An error occurred while creating '{directory_path}': {e}")

def get_first_line(csv):
    f = open(csv, "r")
    s = f.readline()
    result = []
    for x in s.split(","):
        result.append(x.replace(" ", "").replace("\n", ""))
    return result

def longest_common_prefix(strs):
    if not strs:
        return ""

    # Find the shortest string in the list
    shortest_str = min(strs, key=len)

    for i, char in enumerate(shortest_str):
        for string in strs:
            if string[i] != char:
                return shortest_str[:i]
    
    return shortest_str