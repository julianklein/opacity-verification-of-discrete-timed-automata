import os
import sys

from functions import color_text
from functions import list_directories

def main():
    CURRENTPATH = os.getcwd()
    PROJECTPATH = CURRENTPATH[:CURRENTPATH.find("evaluation")]
    OUTPUTEVALUATIONPATH = PROJECTPATH + "output/evaluation"

    print(color_text("Running full plotter with configuration", "cyan"))
    print(color_text("    project path : " + PROJECTPATH, "cyan"))
    print(color_text("    output path  : " + OUTPUTEVALUATIONPATH, "cyan"))

    l = list_directories(OUTPUTEVALUATIONPATH)

    print("detected files: " + str(l))

    for e in l:
        #if (e != "case_studies"):
        s = OUTPUTEVALUATIONPATH + "/" + e + "/out.csv"

        MODE = "plot"

        if (e == "case_studies"):
            MODE = "table"

        if "open" in sys.argv:
            MODE = MODE + " open"

        color_text("python3 plotter.py " + s + " " + MODE, "yellow")
        os.system("python3 plotter.py " + s + " " + MODE)





























if __name__ == "__main__":
	main()
