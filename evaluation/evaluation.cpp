#include <thread>
#include <mutex>
#include <chrono>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <chrono>
#include <fstream>
#include <filesystem>
#include <iostream>

#include "evaluator.h"
#include "synthesis.h"
#include "analysis.h"
#include "automata_reader.h"


std::vector<double> gl_lo_test(std::string mode, int level)
{
    TA ta;

    if (mode == "states")
    {
        ta = synthesis::getRandomTA(1, 1 + level, 1, 1, 1, 10 , 9, 2, 0.25, synthesis::get_random_seed(100), 0, 50, 5, true);
    }
    else if (mode == "time")
    {
        ta = synthesis::getRandomTA(1, 50, 1, 1, 1, 10, 9, 2, 0.25, synthesis::get_random_seed(100), 0, level, 5, true);
    }

    // compute gl abstraction
    auto gl_abs_start = std::chrono::high_resolution_clock::now();

    std::map<unsigned int, std::vector<unsigned int>> gl_abs = analysis::get_global_clock_abstraction_mapped(ta);

    auto gl_abs_end = std::chrono::high_resolution_clock::now();
    std::chrono::microseconds gl_abs_duration = std::chrono::duration_cast<std::chrono::microseconds>(gl_abs_end - gl_abs_start);
    double gl_abs_time = gl_abs_duration.count();

    // compute lo abstraction
    auto lo_abs_start = std::chrono::high_resolution_clock::now();

    std::map<unsigned int, std::vector<unsigned int>> lo_abs = analysis::get_state_local_clock_abstraction(ta);

    auto lo_abs_end = std::chrono::high_resolution_clock::now();
    std::chrono::microseconds lo_abs_duration = std::chrono::duration_cast<std::chrono::microseconds>(lo_abs_end - lo_abs_start);
    double lo_abs_time = lo_abs_duration.count();

    // compute gl fa
    auto gl_fa_start = std::chrono::high_resolution_clock::now();

    FA gl_fa = analysis::get_FA_from_TA(ta, gl_abs);

    auto gl_fa_end = std::chrono::high_resolution_clock::now();
    std::chrono::microseconds gl_fa_duration = std::chrono::duration_cast<std::chrono::microseconds>(gl_fa_end - gl_fa_start);
    double gl_fa_time = gl_fa_duration.count();

    // compute lo fa
    auto lo_fa_start = std::chrono::high_resolution_clock::now();

    FA lo_fa = analysis::get_FA_from_TA(ta, lo_abs);

    auto lo_fa_end = std::chrono::high_resolution_clock::now();
    std::chrono::microseconds lo_fa_duration = std::chrono::duration_cast<std::chrono::microseconds>(lo_fa_end - lo_fa_start);
    double lo_fa_time = lo_fa_duration.count();


    std::vector<double> result;
    result.push_back(double(level));
    result.push_back(gl_abs_time);
    result.push_back(lo_abs_time);
    result.push_back(double(gl_fa.get_number_states()));
    result.push_back(double(lo_fa.get_number_states()));
    result.push_back(gl_fa_time);
    result.push_back(lo_fa_time);
    result.push_back(gl_abs_time + gl_fa_time);
    result.push_back(lo_abs_time + lo_fa_time);
    return result;
}

void run_randomized()
{
    srand(time(NULL));

    Evaluator evaluator_abs_states(
        "tick_automata_states", 
        "states", 10, 100, 1000, gl_lo_test, 
        {"States", 
            "GL_ABS_TIME", "LO_ABS_TIME", "GL_FA_STATES", "LO_FA_STATES", "GL_FA_TIME", "LO_FA_TIME", "GL_TOTAL_TIME", "LO_TOTAL_TIME"
        }
    );

    Evaluator evaluator_abs_time(
        "tick_automata_time", 
        "time", 10, 100, 1000, gl_lo_test, 
        {"MaxTimes", 
            "GL_ABS_TIME", "LO_ABS_TIME", "GL_FA_STATES", "LO_FA_STATES", "GL_FA_TIME", "LO_FA_TIME", "GL_TOTAL_TIME", "LO_TOTAL_TIME"
        }
    );
    evaluator_abs_states.run();
    evaluator_abs_time.run();
}

std::vector<double> case_studies(std::string mode, int level)
{
    TA ta;
    std::vector<double> result;
    std::unordered_set<std::string> observables;

    // cloud
     if (level == 0)
    {
        ta = automata_reader::load_ta_json("case_studies/cloud.json");
        observables = {"aut", "p_c", "to", "c_cp", "pr_cp", "e_cp"};
    }
    // medical
    else if (level == 1)
    {
        ta = automata_reader::load_ta_json("case_studies/medical.json");
        observables = {"phi1", "phi31", "phi32", "phi51", "phi52", "phi61", "phi62"};
    }
    // grid
    else if (level == 2)
    {
        ta = automata_reader::load_ta_json("case_studies/grid.json");
        observables = {"a", "b", "c", "d", "e", "f", "g", "h", "i"};
    }
    // atm
    else if (level == 3)
    {
        ta = automata_reader::load_ta_json("case_studies/atm.json");
        observables = {"pwd", "reqBalance", "quickWdraw", "normalWdraw", "OK", "correctAmount", "takeCash", "restart", "pressFinish"};
    }

    // mask ta
    ta = analysis::mask_ta(ta, observables);


    // compute gl abstraction
    auto gl_abs_start = std::chrono::high_resolution_clock::now();

    std::map<unsigned int, std::vector<unsigned int>> gl_abs = analysis::get_global_clock_abstraction_mapped(ta);

    auto gl_abs_end = std::chrono::high_resolution_clock::now();
    std::chrono::nanoseconds gl_abs_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(gl_abs_end - gl_abs_start);
    double gl_abs_time = gl_abs_duration.count();

    // compute lo abstraction
    auto lo_abs_start = std::chrono::high_resolution_clock::now();

    std::map<unsigned int, std::vector<unsigned int>> lo_abs = analysis::get_state_local_clock_abstraction(ta);

    auto lo_abs_end = std::chrono::high_resolution_clock::now();
    std::chrono::nanoseconds lo_abs_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(lo_abs_end - lo_abs_start);
    double lo_abs_time = lo_abs_duration.count();


    // compute gl fa
    auto gl_fa_start = std::chrono::high_resolution_clock::now();

    FA gl_fa = analysis::get_FA_from_TA(ta, gl_abs);

    auto gl_fa_end = std::chrono::high_resolution_clock::now();
    std::chrono::nanoseconds gl_fa_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(gl_fa_end - gl_fa_start);
    double gl_fa_time = gl_fa_duration.count();


    // compute lo fa
    auto lo_fa_start = std::chrono::high_resolution_clock::now();

    FA lo_fa = analysis::get_FA_from_TA(ta, lo_abs);

    auto lo_fa_end = std::chrono::high_resolution_clock::now();
    std::chrono::nanoseconds lo_fa_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(lo_fa_end - lo_fa_start);
    double lo_fa_time = lo_fa_duration.count();

    // compute gl dfa
    auto gl_dfa_start = std::chrono::high_resolution_clock::now();

    FA gl_dfa = analysis::determinization_FA(gl_fa);

    auto gl_dfa_end = std::chrono::high_resolution_clock::now();
    std::chrono::nanoseconds gl_dfa_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(gl_dfa_end - gl_dfa_start);
    double gl_dfa_time = gl_dfa_duration.count();

    // compute lo dfa
    auto lo_dfa_start = std::chrono::high_resolution_clock::now();

    FA lo_dfa = analysis::determinization_FA(lo_fa);

    auto lo_dfa_end = std::chrono::high_resolution_clock::now();
    std::chrono::nanoseconds lo_dfa_duration = std::chrono::duration_cast<std::chrono::nanoseconds>(lo_dfa_end - lo_dfa_start);
    double lo_dfa_time = lo_dfa_duration.count();

    result.push_back(double(level));
    result.push_back(gl_abs_time/1000000);
    result.push_back(lo_abs_time/1000000);
    result.push_back(double(gl_fa.get_number_states()));
    result.push_back(double(lo_fa.get_number_states()));
    result.push_back(gl_fa_time/1000000);
    result.push_back(lo_fa_time/1000000);
    result.push_back(double(gl_dfa.get_number_states()));
    result.push_back(double(lo_dfa.get_number_states()));
    result.push_back((gl_abs_time + gl_fa_time + gl_dfa_time)/1000000);
    result.push_back((lo_abs_time + lo_fa_time + lo_dfa_time)/1000000);
    return result;
}

void run_case_studies()
{
    Evaluator evaluator_case_studies(
        "case_studies", 
        "case_studies", 1, 4, 10, case_studies, 
        {"Case Studies", 
            "GL_ABS_TIME", "LO_ABS_TIME", "GL_FA_STATES", "LO_FA_STATES", "GL_FA_TIME", "LO_FA_TIME", "GL_DFA_STATES", "LO_DFA_STATES", "GL_DFA_TOTAL_TIME", "LO_DFA_TOTAL_TIME"
        }
    );
    evaluator_case_studies.run();
}

int main(const int argc, const char **argv)
{
    auto start = std::chrono::high_resolution_clock::now();

    // run randomized tests
    run_randomized();

    // run case studies
    run_case_studies();

    auto end = std::chrono::high_resolution_clock::now();
    auto runtime = std::chrono::duration_cast<std::chrono::seconds>(end - start);
    std::cout << "Full evaluation terminated in " << runtime.count() << "s" << std::endl;

    return 0;
}