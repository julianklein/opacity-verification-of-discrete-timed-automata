#include <iostream>
#include <map>
#include <fstream>
#include <chrono>

#include "json/json.h"
#include "utils.h"
#include "analysis.h"
#include "automata_reader.h"

void print_all_automata()
{
    for (std::string name : {"atm", "cloud", "grid", "medical"})
    {
        TA A = automata_reader::load_ta_json("case_studies/" + name + ".json");
        A.print_to_png(std::string(OUTPUT_PATH) + "/" + name);
    }
}
int main(int argc, char *argv[])
{
    auto start = std::chrono::high_resolution_clock::now();

    print_all_automata();

    auto end = std::chrono::high_resolution_clock::now();
    auto runtime = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
    std::cout << "terminated in " << runtime.count() << " ms" << std::endl;
}
