#include "analysis.h"

#define CONFIGURATION std::pair<StateID, ClockValuation>

std::pair<StateID, bool> get_state
(
    const StateID& location, 
    const ClockValuation& valuation, 
    std::unordered_map<CONFIGURATION, StateID>& state_map, 
    StateID& state_index,
    const StateID& A_number_clocks,
    const std::unordered_map<StateID, bool>& A_state_initial,
    const std::unordered_map<StateID, bool>& A_state_accepting,
    const std::unordered_map<StateID, bool>& A_state_secret,
    const std::unordered_map<StateID, StateName>& A_state_name,
    std::unordered_set<StateID>& new_initial_states,
    std::unordered_map<StateID, bool>& new_state_initial,
    std::unordered_map<StateID, bool>& new_state_accepting,
    std::unordered_map<StateID, bool>& new_state_secret,
    std::unordered_map<StateID, StateName>& new_state_name
)
{
    // check if we already discovered this state
    auto find = state_map.find(std::make_pair(location, valuation));
    if (find != state_map.end())
    {
        return std::make_pair(find->second, false);
    }

    // this must be a new state
    state_map[std::make_pair(location, valuation)] = state_index;

    // read out information of original location in TA
    auto initial_find = A_state_initial.find(location);
    auto accepting_find = A_state_accepting.find(location);
    auto secret_find = A_state_secret.find(location);
    auto name_find = A_state_name.find(location);

    if (
        (initial_find != A_state_initial.end()) and 
        (accepting_find != A_state_accepting.end()) and 
        (secret_find != A_state_secret.end()) and 
        (name_find != A_state_name.end())
    )
    {
        // copy values from original TA
        if ((valuation == std::vector<unsigned int>(A_number_clocks, 0)) and (initial_find->second))
        {
            new_initial_states.emplace(state_index);
            new_state_initial[state_index] = true;
        }
        else
        {
            new_state_initial[state_index] = false;
        }
        new_state_accepting[state_index] = accepting_find->second;
        new_state_secret[state_index] = secret_find->second;
        new_state_name[state_index] = "(" + name_find->second + ", " + utils::clockVectorToString(valuation) + ")";
    }
    else
    {
        // we could not find a value for the provided location
        throw std::runtime_error("invalid location maps");
    }

    // increase state index
    state_index++;

    return std::make_pair(state_index - 1, true);
}

FA analysis::get_FA_from_TA(const TA& A, const Abstraction& abstraction)
{
    // read out data from A
    const unsigned int& A_number_clocks = A.get_number_clocks();
    //const unsigned int& A_number_states = A.get_number_states();
    const std::unordered_set<Symbol>& A_symbols = A.get_symbols();
    const std::unordered_set<StateID>& A_initial_states = A.get_initial_states();
    const std::unordered_map<StateID, bool>& A_state_initial = A.get_state_initial();
    const std::unordered_map<StateID, bool>& A_state_accepting = A.get_state_accepting();
    const std::unordered_map<StateID, bool>& A_state_secret = A.get_state_secret();
    const std::unordered_map<StateID, StateName>& A_state_name = A.get_state_name();
    const std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<TATransition>>& A_transitions = A.get_transitions();

    std::unordered_set<StateID> new_initial_states;
    std::unordered_map<StateID, bool> new_state_initial;
    std::unordered_map<StateID, bool> new_state_accepting;
    std::unordered_map<StateID, bool> new_state_secret;
    std::unordered_map<StateID, StateName> new_state_name;
    std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<unsigned int>> new_transitions;

    std::unordered_map<CONFIGURATION, StateID> state_map;
    StateID state_index = 0;

    std::unordered_set<Symbol> symbols_with_epsilon = A_symbols;
    symbols_with_epsilon.emplace(SILENT_SYMBOL);

    std::list<CONFIGURATION> Q;

    std::unordered_set<StateID> time_visited;

    // add all initial states with zero-clock vector to Q
    for (const auto& i : A_initial_states)
    {
        Q.push_back(std::make_pair(i, ClockValuation(A_number_clocks, 0)));
    }
    
    // go through all discovered states
    while (Q.size() > 0)
    {
        // dequeue first element of Q
        CONFIGURATION current = Q.front();
        Q.pop_front();
        StateID current_location = current.first;
        ClockValuation current_valuation = current.second;

        // read out local max clock values
        auto clock_max_find = abstraction.find(current_location);
        if (clock_max_find == abstraction.end())
        {
            throw std::runtime_error("no abstraction for current location");
        }
        ClockValuation local_max_clock_values = clock_max_find->second;

        // evaluate sigma transitions
        std::pair<StateID, bool> origin = get_state(
            current_location,
            get_representative(current_valuation, local_max_clock_values),
            state_map,
            state_index,
            A_number_clocks,
            A_state_initial,
            A_state_accepting,
            A_state_secret,
            A_state_name,
            new_initial_states,
            new_state_initial,
            new_state_accepting,
            new_state_secret,
            new_state_name 
        );

        // for each symbol
        for (const auto& symbol : symbols_with_epsilon)
        {
            // get all transitions
            auto trans_find = A_transitions.find(std::make_pair(current_location, symbol));
            if (trans_find != A_transitions.end())
            {
                std::unordered_set<TATransition> enabled = trans_find->second;
                // for every transition in current location with symbol
                for (const auto& t : enabled)
                {
                    Guard guard = std::get<0>(t);
                    ResetSet resets = std::get<1>(t);
                    StateID destination = std::get<2>(t);

                    // read out local max clock values
                    auto clock_max_find_t = abstraction.find(destination);
                    if (clock_max_find_t == abstraction.end())
                    {
                        throw std::runtime_error("no abstraction for current location");
                    }
                    std::vector<unsigned int> local_max_clock_values_t = clock_max_find_t->second;


                    // if guard is enabled by current valuation
                    if (guard.isEnabledBy(current_valuation))
                    {
                        // get new valuation
                        std::vector<unsigned int> new_valuation = apply_resets(current_valuation, resets);

                        // get target state
                        std::pair<unsigned int, bool> target = get_state(
                            destination,
                            get_representative(new_valuation, local_max_clock_values_t),
                            state_map,
                            state_index,
                            A_number_clocks,
                            A_state_initial,
                            A_state_accepting,
                            A_state_secret,
                            A_state_name,
                            new_initial_states,
                            new_state_initial,
                            new_state_accepting,
                            new_state_secret,
                            new_state_name 
                        );

                        // add transition
                        new_transitions[std::make_pair(origin.first, symbol)].emplace(target.first);

                        if (target.second)
                        {
                            Q.push_back(std::make_pair(destination, get_representative(new_valuation, local_max_clock_values_t)));
                        }
                    }
                }
            }
        }

        // evaluate time transitions
        ClockValuation v = current_valuation;
        ClockValuation last = v;
        bool initial = true;
        while ((get_representative(v, local_max_clock_values) != get_representative(last, local_max_clock_values)) or initial)
        {
            initial = false;
            last = v;
            std::pair<unsigned int, bool> origin = get_state(
                current_location,
                get_representative(v, local_max_clock_values),
                state_map,
                state_index,
                A_number_clocks,
                A_state_initial,
                A_state_accepting,
                A_state_secret,
                A_state_name,
                new_initial_states,
                new_state_initial,
                new_state_accepting,
                new_state_secret,
                new_state_name 
            );

            // v = v + 1
            v = advance_by_one_vec(v);
            
            std::pair<unsigned int, bool> target = get_state(
                current_location,
                get_representative(v, local_max_clock_values),
                state_map,
                state_index,
                A_number_clocks,
                A_state_initial,
                A_state_accepting,
                A_state_secret,
                A_state_name,
                new_initial_states,
                new_state_initial,
                new_state_accepting,
                new_state_secret,
                new_state_name 
            );

            // add tick transition
            new_transitions[std::make_pair(origin.first, TICK_SYMBOL)].emplace(target.first);

            // enqueue if new
            if (target.second)
            {
                Q.push_back(std::make_pair(current_location, get_representative(v, local_max_clock_values)));
            }

            // current configuration has been evaluated before, break
            if (time_visited.find(target.first) != time_visited.end())
            {
                break;
            }

            time_visited.emplace(target.first);
        }
    }

    // add tick symbol to alphabet
    std::unordered_set<Symbol> new_symbols = A_symbols;
    new_symbols.emplace(TICK_SYMBOL);

    FA result = FA(
        state_index,
        new_initial_states,
        new_symbols,
        new_state_accepting,
        new_state_secret,
        new_state_initial,
        new_state_name,
        new_transitions
    );
    return result;
}