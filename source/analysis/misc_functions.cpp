#include "analysis.h"

Word analysis::timed_word_to_word(const TimedWord& tw)
{
    Word result;
    for (const auto& a : tw)
    {
        Symbol symbol = a.first;
        TimeStamp time = a.second;
        for (unsigned int i = 0; i < time; i++)
        {
            result.push_back(TICK_SYMBOL);
        }
        result.push_back(symbol);
    }
    return result;
}

ClockValuation analysis::get_local_max_clock_values(const TA& A, const StateID state)
{
    std::unordered_set<TATransition> enabled = A.get_transitions_of_state(state);
    ClockValuation max_clock_values (A.get_number_clocks(), 0);

    for (auto t : enabled)
    {
        Guard g = std::get<0>(t);
        unsigned int clock_index = 0;
        for (auto ex : g.getExpressions())
        {
            unsigned int max_from_expression = ex.getMaxConstant();
            
            if (max_from_expression > max_clock_values[clock_index])
            {
                max_clock_values[clock_index] = max_from_expression;
            }
            clock_index = clock_index + 1;
        }
    }
    return max_clock_values;
}

Abstraction analysis::get_state_local_clock_abstraction(const TA& A)
{
    Abstraction result;

    unsigned int number_states = A.get_number_states();
    unsigned int number_clocks = A.get_number_clocks();

    for (StateID s = 0; s < number_states; s++)
    {
        ClockValuation local (A.get_number_clocks(), 0);

        for (unsigned int c = 0; c < number_clocks; c++)
        {
            unsigned int max_value = 0;
            std::unordered_set<unsigned int> reach;
            std::list<unsigned int> queue;
            queue.push_back(s);
            reach.emplace(s);
            
            while (queue.size() > 0)
            {
                StateID current = queue.front();
                queue.pop_front();

                ClockValuation loc = get_local_max_clock_values(A, current);

                if (loc[c] > max_value)
                {
                    max_value = loc[c];
                }

                std::unordered_set<TATransition> enabled = A.get_transitions_of_state(current);

                for (auto e : enabled)
                {
                    std::unordered_set<unsigned int> resets = std::get<1>(e);
                    if (resets.find(c) == resets.end())
                    {
                        // c does not get reset on this transition 
    
                        Guard guard = std::get<0>(e);
                        if (guard.getExpressions()[c].isOpen())
                        {
                            StateID target = std::get<2>(e);
                            if (reach.find(target) == reach.end())
                            {
                                reach.emplace(target);
                                queue.push_back(target);
                            }
                        }
                    }
                }
            }
            local[c] = max_value;
        }

        result[s] = local;
    }
    return result;
}

ClockValuation analysis::get_representative(const ClockValuation& valuation, const ClockValuation& max_clocks)
{
    ClockValuation result (max_clocks.size(), 0);
    for (decltype(max_clocks.size()) i = 0; i < max_clocks.size(); i++)
    {
        if (valuation[i] > max_clocks[i])
        {
            result[i] = max_clocks[i] + 1;
        }
        else
        {
            result[i] = valuation[i];
        }
    }
    return result;
}

ClockValuation analysis::advance_by_one_vec(const ClockValuation& clocks)
{
    ClockValuation new_clocks (clocks.size(), 0);
    for (decltype(clocks.size()) i = 0; i < clocks.size(); i++)
    {
        new_clocks[i] = clocks[i] + 1;
    }
    return new_clocks;
}

ClockValuation analysis::apply_resets(const ClockValuation& valuation, const ResetSet& resets)
{
    ClockValuation result (valuation.size(), 0);
    for (decltype(valuation.size()) i = 0; i < valuation.size(); i++)
    {
        if (resets.find(i) != resets.end())
        {
            result[i] = 0;
        }
        else
        {
            result[i] = valuation[i];
        }
    }
    return result;
}

ClockValuation analysis::get_global_clock_abstraction(const TA& A)
{
    ClockValuation result (A.get_number_clocks(), 0);

    unsigned int number_states = A.get_number_states();
    
    for (unsigned int s = 0; s < number_states; s++)
    {
        ClockValuation loc = get_local_max_clock_values(A, s);

        for (unsigned int i = 0; i < result.size(); i++)
        {
            if (loc[i] > result[i])
            {
                result[i] = loc[i];
            }
        }
    }
    return result;
}

Abstraction analysis::get_global_clock_abstraction_mapped(const TA& A)
{
    ClockValuation global = get_global_clock_abstraction(A);
    Abstraction result;
    const unsigned int& number_states = A.get_number_states();
    for (unsigned int i = 0; i < number_states; i++)
    {
        result[i] = global;
    }
    return result;
}
