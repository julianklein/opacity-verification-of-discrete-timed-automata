#include "fa.h"
#include "ta.h"

/*
Contains a name space with the main functionality, presented in our submission.
*/

namespace analysis
{
    // transform a given TA with a given abstraction to a tau-FA
    FA get_FA_from_TA(const TA& A, const Abstraction& abstraction);
    
    // determinize a given FA
    FA determinization_FA(const FA& A);
    
    // mask a given TA (turn unobservable symbols into epsilon)
    TA mask_ta(const TA& A, const std::unordered_set<Symbol>& observables);

    // turn a timed word to a tick word
    Word timed_word_to_word(const TimedWord& tw);

    // compute local max. constants in a state
    ClockValuation get_local_max_clock_values(const TA& A, const StateID state);
    
    // compute a state-local time abstraction (to transform TA to tau-FA)
    Abstraction get_state_local_clock_abstraction(const TA& A);
    
    // compute a global time abstraction (region abstraction)
    ClockValuation get_global_clock_abstraction(const TA& A);

    // compute a global time abstraction in a mapped format to be used by our transformation algorithm
    Abstraction get_global_clock_abstraction_mapped(const TA& A);

    // given a clock valuation and an abstraction, compute a unique representative
    ClockValuation get_representative(const ClockValuation& valuation, const ClockValuation& max_clocks);
    
    // advance a clock valuation by one
    ClockValuation advance_by_one_vec(const ClockValuation& clocks);

    // given a clock valuation and a set of clocks to be reset, apply the resets and return the resulting clock valuation
    ClockValuation apply_resets(const ClockValuation& valuation, const ResetSet& resets);
}