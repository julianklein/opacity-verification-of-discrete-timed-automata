#include "analysis.h"

TA analysis::mask_ta(const TA& A, const std::unordered_set<Symbol>& observables)
{
    unsigned int number_states_ = A.get_number_states();
    unsigned int number_clocks_ = A.get_number_clocks();
    std::unordered_set<StateID> initial_states_ = A.get_initial_states();
    const std::unordered_set<Symbol>& symbols = A.get_symbols();
    std::unordered_map<StateID, bool> state_accepting_ = A.get_state_accepting();
    std::unordered_map<StateID, bool> state_secret_ = A.get_state_secret();
    std::unordered_map<StateID, bool> state_initial_ = A.get_state_initial();
    std::unordered_map<StateID, std::string> state_name_ = A.get_state_name();

    const std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<TATransition>>& transitions = A.get_transitions();

    std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<TATransition>> transitions_;
    std::unordered_set<Symbol> symbols_ = observables;

    for (StateID i = 0; i < number_states_; i++)
    {
        for (const auto& symbol : symbols)
        {
            auto trans_find = transitions.find(std::make_pair(i, symbol));
            if (trans_find != transitions.end())
            {
                std::unordered_set<TATransition> enabled = trans_find->second;
                for (const auto& e : enabled)
                {
                    if (observables.find(symbol) != observables.end())
                    {
                        transitions_[std::make_pair(i, symbol)].emplace(e);
                    }
                    else
                    {
                        transitions_[std::make_pair(i, SILENT_SYMBOL)].emplace(e);
                    }
                }
            }
        }
    }

    return TA(
        number_states_,
        number_clocks_,
        initial_states_,
        symbols_,
        state_accepting_,
        state_secret_,
        state_initial_,
        state_name_,
        transitions_
    );
}