#include "analysis.h"

std::pair<StateID, bool> get_state_det
(
    const std::unordered_set<StateID> states,
    std::unordered_map<std::unordered_set<StateID>, StateID>& state_map, 
    StateID& state_index,
    const std::unordered_map<StateID, bool>& A_state_initial,
    const std::unordered_map<StateID, bool>& A_state_accepting,
    const std::unordered_map<StateID, bool>& A_state_secret,
    const std::unordered_map<StateID, StateName>& A_state_name,
    std::unordered_set<StateID>& new_initial_states,
    std::unordered_map<StateID, bool>& new_state_initial,
    std::unordered_map<StateID, bool>& new_state_accepting,
    std::unordered_map<StateID, bool>& new_state_secret,
    std::unordered_map<StateID, StateName>& new_state_name
)
{
    // check if we already discovered this state
    auto find = state_map.find(states);
    if (find != state_map.end())
    {
        return std::make_pair(find->second, false);
    }

    // this must be a new state
    state_map[states] = state_index;

    // read out information of original location in FA
    bool accepting = false;
    bool secret = states.size() > 0;
    StateName name = "";

    // create new state properties
    for (const auto& state : states)
    {
        auto initial_find = A_state_initial.find(state);
        auto accepting_find = A_state_accepting.find(state);
        auto secret_find = A_state_secret.find(state);
        auto name_find = A_state_name.find(state);

        if (
            (initial_find != A_state_initial.end()) and 
            (accepting_find != A_state_accepting.end()) and 
            (secret_find != A_state_secret.end()) and 
            (name_find != A_state_name.end())
        )
        {
            // copy values from original FA
            accepting = accepting or accepting_find->second;
            secret = secret and secret_find->second;
            name = name + name_find->second;
        }
        else
        {
            // we could not find a value for the provided state
            throw std::runtime_error("invalid state maps");
        }
    }
    new_state_initial[state_index] = false; // identify initial states in main procedure
    new_state_accepting[state_index] = accepting;
    new_state_secret[state_index] = secret;
    new_state_name[state_index] = name;

    // increase state index
    state_index++;
    return std::make_pair(state_index - 1, true);
}

FA analysis::determinization_FA(const FA& A)
{
    const std::unordered_set<StateID>& A_initial_states = A.get_initial_states();
    const std::unordered_set<Symbol>& A_symbols = A.get_symbols();
    const std::unordered_map<StateID, bool>& A_state_accepting = A.get_state_accepting();
    const std::unordered_map<StateID, bool>& A_state_secret = A.get_state_secret();
    const std::unordered_map<StateID, bool>& A_state_initial = A.get_state_initial();
    const std::unordered_map<StateID, StateName>& A_state_name = A.get_state_name();
    const std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<StateID>>& A_transitions = A.get_transitions();

    std::unordered_set<StateID> new_initial_states;
    std::unordered_set<Symbol> new_symbols;
    std::unordered_map<StateID, bool> new_state_accepting;
    std::unordered_map<StateID, bool> new_state_secret;
    std::unordered_map<StateID, bool> new_state_initial;
    std::unordered_map<StateID, StateName> new_state_name;
    std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<StateID>> new_transitions;

    // state tracking data structures
    StateID state_index = 0;
    std::unordered_map<std::unordered_set<StateID>, StateID> state_map;

    // get first state
    std::unordered_set<StateID> initial = A_initial_states;
    A.epsilonClosure(initial);

    // create initial state
    std::pair<StateID, bool> initial_state = get_state_det
    (
        initial,
        state_map,
        state_index,
        A_state_initial,
        A_state_accepting,
        A_state_secret,
        A_state_name,
        new_initial_states,
        new_state_initial,
        new_state_accepting,
        new_state_secret,
        new_state_name 
    );

    new_state_initial[initial_state.first] = true;
    new_initial_states.emplace(initial_state.first);

    // initialize Q 
    std::list<std::unordered_set<StateID>> Q;
    Q.push_back(initial);

    // while Q is not empty
    while (Q.size() > 0)
    {
        // dequeue first element
        std::unordered_set<StateID> current = Q.front();
        Q.pop_front();

        // get state of current set
        std::pair<StateID, bool> current_state = get_state_det
        (
            current,
            state_map,
            state_index,
            A_state_initial,
            A_state_accepting,
            A_state_secret,
            A_state_name,
            new_initial_states,
            new_state_initial,
            new_state_accepting,
            new_state_secret,
            new_state_name 
        );

        // for each symbol
        for (const auto& symbol : A_symbols)
        {
            // assume we never encounter the silent symbol

            // collect all target states
            std::unordered_set<StateID> targets;
            for (const auto& s : current)
            {
                // determine enabled transitions
                auto transition_find = A_transitions.find(std::make_pair(s, symbol));
                if (transition_find != A_transitions.end())
                {
                    auto enabled = transition_find->second;
                    for (const auto& e : enabled)
                    {
                        // collect target state
                        targets.emplace(e);
                    }
                }
            }

            // compute epsilon closure of collected targets
            A.epsilonClosure(targets);

            // identify target state
            std::pair<StateID, bool> target = get_state_det
            (
                targets,
                state_map,
                state_index,
                A_state_initial,
                A_state_accepting,
                A_state_secret,
                A_state_name,
                new_initial_states,
                new_state_initial,
                new_state_accepting,
                new_state_secret,
                new_state_name 
            );

            // add new transition for current symbol
            new_transitions[std::make_pair(current_state.first, symbol)].emplace(target.first);

            // if target has not been visited yet
            if (target.second)
            {
                // if not in Q, enqueue
                if (std::find(Q.begin(), Q.end(), targets) == Q.end())
                {
                    Q.push_back(targets);
                }
            }
        }
    }


    FA result = FA(
        state_index,
        new_initial_states,
        A_symbols,
        new_state_accepting,
        new_state_secret,
        new_state_initial,
        new_state_name,
        new_transitions
    );

    return result;
}