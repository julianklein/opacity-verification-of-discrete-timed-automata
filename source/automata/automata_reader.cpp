#include "automata_reader.h"

using JSON_State = std::tuple<std::string, bool, bool, bool>;
using JSON_FATransition = std::tuple<std::string, std::string, std::string>;
using JSON_TATransition = std::tuple<std::string, std::string, std::unordered_set<std::tuple<unsigned int, unsigned int, unsigned int>>, std::unordered_set<unsigned int>, std::string>;


FA getFA(std::unordered_set<JSON_State> states, std::unordered_set<JSON_FATransition> transitions)
{
    unsigned int number_states_;
    std::unordered_set<unsigned int> initial_states_;
    std::unordered_set<std::string> symbols_;
    std::unordered_map<StateID, bool> state_accepting_;
    std::unordered_map<StateID, bool> state_secret_;
    std::unordered_map<StateID, bool> state_initial_;
    std::unordered_map<StateID, std::string> state_name_;
    std::unordered_map<std::pair<StateID, std::string>, std::unordered_set<StateID>> transitions_;

    std::unordered_map<std::string, unsigned int> name_to_id;
    unsigned int i = 0;
    std::unordered_set<std::string> state_names;

    for (const auto& s : states)
    {
        std::string name = std::get<0>(s);
        bool initial = std::get<1>(s);
        bool accepting = std::get<2>(s);
        bool secret = std::get<3>(s);
            
        state_name_[i] = name;
        state_secret_[i] = secret;
        state_accepting_[i] = accepting;
        state_initial_[i] = initial;

        name_to_id[name] = i;

        if (initial)
        {
            initial_states_.emplace(i);
        }

        if (state_names.find(name) != state_names.end())
        {
            throw std::runtime_error("state name already used");
        }

        state_names.emplace(name);

        i = i + 1;
    }

    number_states_ = i;

    for (const auto& t : transitions)
    {
        std::string origin = std::get<0>(t);
        std::string symbol = std::get<1>(t);
        std::string target = std::get<2>(t);

        if (state_names.find(origin) == state_names.end())
        {
            throw std::runtime_error("did not find origin name");
        }

        symbols_.emplace(symbol);

        if (state_names.find(target) == state_names.end())
        {
            throw std::runtime_error("did not find target name");
        }
            
        transitions_[std::make_pair(name_to_id[origin], symbol)].emplace(name_to_id[target]);
    }
    return FA(number_states_, initial_states_, symbols_, state_accepting_, state_secret_, state_initial_, state_name_, transitions_);
}

FA automata_reader::load_fa_json(const std::string filename)
{
    std::string path = std::string(INPUT_PATH) + std::string("/") + filename;
    // Read the JSON file
    std::ifstream inputFile(path);
    if (!inputFile.is_open()) {
        std::cerr << "Failed to open JSON file: " << path << std::endl;
        exit(0);
    }

    Json::Value jsonData;
    Json::CharReaderBuilder reader;
    std::string errors;
    try {
        Json::parseFromStream(reader, inputFile, &jsonData, &errors);
    } catch (const Json::Exception& e) {
        std::cerr << "Error reading JSON file: " << e.what() << std::endl;
        exit(0);
    }

    std::unordered_set<JSON_State> states;
    std::unordered_set<JSON_FATransition> transitions;

    const Json::Value& statesJson = jsonData["states"];
    for (const auto& stateJson : statesJson)
    {
        std::string name = stateJson["name"].asString();
        bool initial = stateJson["initial"].asBool();
        bool accepting = stateJson["accepting"].asBool();
        bool secret = stateJson["secret"].asBool();

        states.emplace(std::make_tuple(name, initial, accepting, secret));
    }


    const Json::Value& transitionsJson = jsonData["transitions"];
    for (const auto& transitionJson : transitionsJson)
    {
        std::string origin = transitionJson["origin"].asString();
        std::string symbol = transitionJson["symbol"].asString();
        std::string target = transitionJson["target"].asString();

        transitions.emplace(std::make_tuple(origin, symbol, target));
    }

    unsigned int number_states_t;
    std::unordered_set<unsigned int> initial_states_t;
    std::unordered_set<Symbol> symbols_t;
    std::unordered_map<StateID, bool> state_accepting_t;
    std::unordered_map<StateID, bool> state_secret_t;
    std::unordered_map<StateID, bool> state_initial_t;
    std::unordered_map<StateID, StateName> state_name_t;
    std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<StateID>> transitions_t;

    std::unordered_map<std::string, unsigned int> name_to_id;
    unsigned int i = 0;
    std::unordered_set<std::string> state_names_t;

    for (const auto& s : states)
    {
        std::string name = std::get<0>(s);
        bool initial = std::get<1>(s);
        bool accepting = std::get<2>(s);
        bool secret = std::get<3>(s);
            
        state_name_t[i] = name;
        state_secret_t[i] = secret;
        state_accepting_t[i] = accepting;
        state_initial_t[i] = initial;

        name_to_id[name] = i;

        if (initial)
        {
            initial_states_t.emplace(i);
        }

        if (state_names_t.find(name) != state_names_t.end())
        {
            throw std::runtime_error("state name already used");
        }

        state_names_t.emplace(name);

        i = i + 1;
    }

    number_states_t = i;

    for (const auto& t : transitions)
    {
        std::string origin = std::get<0>(t);
        std::string symbol = std::get<1>(t);
        std::string target = std::get<2>(t);

        if (state_names_t.find(origin) == state_names_t.end())
        {
            throw std::runtime_error("did not find origin name");
        }

        symbols_t.emplace(symbol);

        if (state_names_t.find(target) == state_names_t.end())
        {
            throw std::runtime_error("did not find target name");
        }
        
        transitions_t[std::make_pair(name_to_id[origin], symbol)].emplace(name_to_id[target]);
    }

    return FA(number_states_t, initial_states_t, symbols_t, state_accepting_t, state_secret_t, state_initial_t, state_name_t, transitions_t);
}

TA getTA(unsigned int number_clocks, std::unordered_set<JSON_State> states, std::unordered_set<JSON_TATransition> transitions)
{
    unsigned int number_states_;
    unsigned int number_clocks_;
    std::unordered_set<StateID> initial_states_;
    std::unordered_set<Symbol> symbols_;
    std::unordered_map<StateID, bool> state_accepting_;
    std::unordered_map<StateID, bool> state_secret_;
    std::unordered_map<StateID, bool> state_initial_;
    std::unordered_map<StateID, StateName> state_name_;
    std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<TATransition>> transitions_;

    number_clocks_ = number_clocks;
    std::unordered_map<std::string, unsigned int> name_to_id;
    unsigned int i = 0;

    std::unordered_set<std::string> symbol_names;
    std::unordered_set<std::string> state_names;

    for (const auto& s : states)
    {
        std::string name = std::get<0>(s);
        bool initial = std::get<1>(s);
        bool accepting = std::get<2>(s);
        bool secret = std::get<3>(s);
            
        state_name_[i] = name;
        state_secret_[i] = secret;
        state_accepting_[i] = accepting;
        state_initial_[i] = initial;

        name_to_id[name] = i;

        if (initial)
        {
            initial_states_.emplace(i);
        }

        if (state_names.find(name) != state_names.end())
        {
            std::cout << "this should never happen 3568738976290468305976390468" << std::endl;
            exit(0);
        }

        state_names.emplace(name);

        i = i + 1;
    }
    number_states_ = i;

    for (const auto& t : transitions)
    {
        std::string origin = std::get<0>(t);
        std::string symbol = std::get<1>(t);
        std::unordered_set<std::tuple<unsigned int, unsigned int, unsigned int>> guards = std::get<2>(t);
        std::unordered_set<unsigned int> resets = std::get<3>(t);
        std::string target = std::get<4>(t);
        if (symbol != SILENT_SYMBOL)
        {
            symbols_.emplace(symbol);
        }
        
        if (state_names.find(origin) == state_names.end())
        {
            std::cout << "this should never happen 8934759824785264574905443652462476906" << std::endl;
            std::cout << origin << std::endl;
            exit(0);
        }
        if (state_names.find(target) == state_names.end())
        {
            std::cout << "this should never happen 5765897689357689235697635636" << std::endl;
            std::cout << target << std::endl;
            exit(0);
        }

        std::unordered_map<unsigned int, std::unordered_set<std::pair<unsigned int, unsigned int>>> sorted_guards;

        for (const auto& g : guards)
        {
            unsigned int clock_index = std::get<0>(g);
            sorted_guards[clock_index].emplace(std::make_pair(std::get<1>(g), std::get<2>(g)));
        }

        std::vector<IntervalExpression> iexpressions;
        for (unsigned int clock_index = 0; clock_index < number_clocks; clock_index++)
        {
            std::vector<std::pair<unsigned int, bool>> intervals;
            for (const auto& g : sorted_guards[clock_index])
            {
                unsigned int lower = std::get<0>(g);
                unsigned int upper = std::get<1>(g);
                std::pair<unsigned int, unsigned int> left = std::make_pair(lower, true);
                std::pair<unsigned int, unsigned int> right = std::make_pair(upper, false);
                intervals.push_back(left);
                intervals.push_back(right);
            }
            IntervalExpression iexpression = IntervalExpression(intervals);
            if (intervals.size() == 0)
            {
                iexpression = IntervalExpression({std::make_pair(0, true)});
            }
            iexpressions.push_back(iexpression);
        }
            
        transitions_[std::make_pair(name_to_id[origin], symbol)].emplace(std::make_tuple(Guard(iexpressions), resets, name_to_id[target]));
    }
    return TA(number_states_, number_clocks_, initial_states_, symbols_, state_accepting_, state_secret_, state_initial_, state_name_, transitions_);
}

TA automata_reader::load_ta_json(const std::string filename)
{
    std::string path = std::string(INPUT_PATH) + std::string("/") + filename;
    // Read the JSON file
    std::ifstream inputFile(path);
    if (!inputFile.is_open()) {
        std::cerr << "Failed to open JSON file: " << path << std::endl;
        exit(0);
    }

    Json::Value jsonData;
    Json::CharReaderBuilder reader;
    std::string errors;
    try {
        Json::parseFromStream(reader, inputFile, &jsonData, &errors);
    } catch (const Json::Exception& e) {
        std::cerr << "Error reading JSON file: " << e.what() << std::endl;
        exit(0);
    }
    
    std::unordered_set<JSON_State> states;
    std::unordered_set<JSON_TATransition> transitions;

    unsigned int number_clocks = jsonData["clocks"].asUInt();

    const Json::Value& statesJson = jsonData["states"];
    for (const auto& stateJson : statesJson)
    {
        std::string name = stateJson["name"].asString();
        bool initial = stateJson["initial"].asBool();
        bool accepting = stateJson["accepting"].asBool();
        bool secret = stateJson["secret"].asBool();

        states.emplace(std::make_tuple(name, initial, accepting, secret));
    }


    const Json::Value& transitionsJson = jsonData["transitions"];
    for (const auto& transitionJson : transitionsJson)
    {
        std::string origin = transitionJson["origin"].asString();
        std::string symbol = transitionJson["symbol"].asString();
        std::string target = transitionJson["target"].asString();

        std::unordered_set<unsigned int> resets;
        const Json::Value &resetsArray = transitionJson["resets"];
        for (const Json::Value &reset : resetsArray)
        {
            if (reset.isUInt())
            {
                resets.insert(reset.asUInt());
            }
        }
        std::unordered_set<std::tuple<unsigned int, unsigned int, unsigned int>> guards;
        const Json::Value &guardsArray = transitionJson["guards"];
        for (const Json::Value &guard : guardsArray)
        {
            unsigned int clock = guard["clock"].asUInt();
            unsigned int lower = guard["lower"].asUInt();
            unsigned int upper = guard["upper"].asUInt();

            guards.emplace(std::make_tuple(clock, lower, upper));
        }


        transitions.emplace(std::make_tuple(origin, symbol, guards, resets, target));
    }

    unsigned int number_states_t;
    unsigned int number_clocks_t;
    std::unordered_set<StateID> initial_states_t;
    std::unordered_set<Symbol> symbols_t;
    std::unordered_map<StateID, bool> state_accepting_t;
    std::unordered_map<StateID, bool> state_secret_t;
    std::unordered_map<StateID, bool> state_initial_t;
    std::unordered_map<StateID, StateName> state_name_t;
    std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<TATransition>> transitions_t;

    number_clocks_t = number_clocks;
    std::unordered_map<std::string, unsigned int> name_to_id;
    unsigned int i = 0;

    std::unordered_set<std::string> symbol_names;
    std::unordered_set<std::string> state_names;

    for (const auto& s : states)
    {
        std::string name = std::get<0>(s);
        bool initial = std::get<1>(s);
        bool accepting = std::get<2>(s);
        bool secret = std::get<3>(s);
            
        state_name_t[i] = name;
        state_secret_t[i] = secret;
        state_accepting_t[i] = accepting;
        state_initial_t[i] = initial;

        name_to_id[name] = i;

        if (initial)
        {
            initial_states_t.emplace(i);
        }

        if (state_names.find(name) != state_names.end())
        {
            std::cout << "this should never happen 3568738976290468305976390468" << std::endl;
            exit(0);
        }

        state_names.emplace(name);

        i = i + 1;
    }
    number_states_t = i;

    for (const auto& t : transitions)
    {
        std::string origin = std::get<0>(t);
        std::string symbol = std::get<1>(t);
        std::unordered_set<std::tuple<unsigned int, unsigned int, unsigned int>> guards = std::get<2>(t);
        std::unordered_set<unsigned int> resets = std::get<3>(t);
        std::string target = std::get<4>(t);

        if (symbol != SILENT_SYMBOL)
        {
            symbols_t.emplace(symbol);
        }
            
        if (state_names.find(origin) == state_names.end())
        {
            std::cout << "this should never happen 8934759824785264574905443652462476906" << std::endl;
            std::cout << origin << std::endl;
            exit(0);
        }

        if (state_names.find(target) == state_names.end())
        {
            std::cout << "this should never happen 5765897689357689235697635636" << std::endl;
            std::cout << target << std::endl;
            exit(0);
        }

        std::unordered_map<unsigned int, std::unordered_set<std::pair<unsigned int, unsigned int>>> sorted_guards;

        for (const auto& g : guards)
        {
            unsigned int clock_index = std::get<0>(g);
            sorted_guards[clock_index].emplace(std::make_pair(std::get<1>(g), std::get<2>(g)));
        }

        std::vector<IntervalExpression> iexpressions;
        for (unsigned int clock_index = 0; clock_index < number_clocks; clock_index++)
        {
            std::vector<std::pair<unsigned int, bool>> intervals;
         
            for (const auto& g : sorted_guards[clock_index])
            {
                unsigned int lower = std::get<0>(g);
                unsigned int upper = std::get<1>(g);

                std::pair<unsigned int, unsigned int> left = std::make_pair(lower, true);
                std::pair<unsigned int, unsigned int> right = std::make_pair(upper, false);
                intervals.push_back(left);
                intervals.push_back(right);
            }
            IntervalExpression iexpression = IntervalExpression(intervals);
            
            if (intervals.size() == 0)
            {
                iexpression = IntervalExpression({std::make_pair(0, true)});
            }
            iexpressions.push_back(iexpression);
        }
            
        transitions_t[std::make_pair(name_to_id[origin], symbol)].emplace(std::make_tuple(Guard(iexpressions), resets, name_to_id[target]));
    }

    return TA(number_states_t, number_clocks_t, initial_states_t, symbols_t, state_accepting_t, state_secret_t, state_initial_t, state_name_t, transitions_t);
}