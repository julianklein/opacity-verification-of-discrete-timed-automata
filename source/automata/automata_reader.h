#ifndef AUTOMATA_READER_H_
#define AUTOMATA_READER_H_

#include <fstream>
#include <list>
#include <map>
#include <unordered_map>

#include "fa.h"
#include "ta.h"
#include "constants.h"
#include "json/json.h"

/*
Contains a name space with functionality to read automata from json-files.
*/

namespace std
{
    template <>
    struct hash<std::tuple<std::string, bool, bool, bool>>
    {
        size_t operator()(const std::tuple<std::string, bool, bool, bool>& x) const
        {
            return hash<std::string>()(std::get<0>(x) + ", " + std::to_string(std::get<1>(x)) + ", " + std::to_string(std::get<2>(x)) + ", " + std::to_string(std::get<3>(x)));
        }
    };

    template <>
    struct hash<std::tuple<std::string, std::string, std::string>>
    {
        size_t operator()(const std::tuple<std::string, std::string, std::string>& x) const
        {
            return hash<std::string>()(std::get<0>(x) + ", " + std::get<1>(x) + ", " + std::get<2>(x));
        }
    };

    inline
    std::string triple_int_set_to_string(std::unordered_set<std::tuple<unsigned int, unsigned int, unsigned int>> s)
    {
        std::string result = "";
        for (const auto& i : s)
        {
            result = result + "(" + std::to_string(std::get<0>(i)) + ", " + std::to_string(std::get<1>(i)) + ", " + std::to_string(std::get<2>(i)) + ")";
        }
        return result;
    };

    template <>
    struct hash<std::tuple<std::string, std::string, std::unordered_set<std::tuple<unsigned int, unsigned int, unsigned int>>, std::unordered_set<unsigned int>, std::string>>
    {
        size_t operator()(const std::tuple<std::string, std::string, std::unordered_set<std::tuple<unsigned int, unsigned int, unsigned int>>, std::unordered_set<unsigned int>, std::string>& x) const
        {
            return hash<std::string>()(std::get<0>(x) + ", " + std::get<1>(x) + ", " + triple_int_set_to_string(std::get<2>(x)) + ", " + utils::containerToString(std::get<3>(x)) + ", " + std::get<4>(x));
        }
    };
}

namespace automata_reader
{
    FA load_fa_json(const std::string path);
    TA load_ta_json(const std::string path);
}

#endif // AUTOMATA_READER_H_