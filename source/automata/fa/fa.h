#ifndef FA_H_
#define FA_H_

#include <fstream>
#include <list>
#include <map>
#include <unordered_map>

#include "utils.h"
#include "constants.h"

/*
Contains the class FA, representing finite automata (FA). The class provides a structure for FA, as well as functionality
for the semantics (for reading input words) and storing to files (dot-files and png-files).
*/

class FA
{

private:
    // number of states
    unsigned int number_states_;

    // set of indices of initial states
    std::unordered_set<StateID> initial_states_;

    // input alphabet
    std::unordered_set<Symbol> symbols_;

    // state properties
    std::unordered_map<StateID, bool> state_accepting_;
    std::unordered_map<StateID, bool> state_secret_;
    std::unordered_map<StateID, bool> state_initial_;
    std::unordered_map<StateID, StateName> state_name_;

    // transition function
    std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<StateID>> transitions_;

public:

    // empty constructor
    FA() = default;

    // stanadrd constructor
    FA(
        unsigned int number_states,
        std::unordered_set<unsigned int> initial_states,
        std::unordered_set<Symbol> symbols,
        std::unordered_map<StateID, bool> state_accepting,
        std::unordered_map<StateID, bool> state_secret,
        std::unordered_map<StateID, bool> state_initial,
        std::unordered_map<StateID, StateName> state_name,
        std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<StateID>> transitions
    )
    {
        number_states_ = number_states;
        initial_states_ = initial_states;
        symbols_ = symbols;
        state_accepting_ = state_accepting;
        state_secret_ = state_secret;
        state_initial_ = state_initial;
        state_name_ = state_name;
        transitions_ = transitions;
    }

    /*
        ========================== get methods ==========================
    */

    const unsigned int& get_number_states() const
    {
        return number_states_;
    }
    
    const std::unordered_set<StateID>& get_initial_states() const
    {
        return initial_states_;
    }

    const std::unordered_set<Symbol>& get_symbols() const
    {
        return symbols_;
    }

    const std::unordered_map<StateID, bool>& get_state_accepting() const
    {
        return state_accepting_;
    }

    const std::unordered_map<StateID, bool>& get_state_secret() const
    {
        return state_secret_;
    }

    const std::unordered_map<StateID, bool>& get_state_initial() const
    {
        return state_initial_;
    }
    
    const std::unordered_map<StateID, StateName>& get_state_name() const
    {
        return state_name_;
    }

    const std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<StateID>>& get_transitions() const
    {
        return transitions_;
    }

    /*
        ========================== evaluation methods ==========================
    */

    // returns true if this FA contains a secret state
    bool has_secret_state() const
    {
        for (const auto& i : state_secret_)
        {
            if (i.second)
            {
                return true;
            }
        }
        return false;
    }

    // computes the epsilon closure of a set of states
    void epsilonClosure(std::unordered_set<StateID>& states) const
    {
        for (auto s : states)
        {
            auto trans_find = transitions_.find(std::make_pair(s, SILENT_SYMBOL));
            if (trans_find != transitions_.end())
            {
                const std::unordered_set<StateID> l = trans_find->second;
                for (auto t : l)
                {
                    if (states.find(t) == states.end())
                    {
                        states.emplace(t);
                        epsilonClosure(states);
                    }
                }
            }
        }
    }

    // return true if the input word is accepted
    bool evaluate(const Word& word) const
    {
        // edge case for empty state set
        if (number_states_ == 0)
        {
            return false;
        }

        // get epsilon closure of all initial states
        std::unordered_set<StateID> init_states = initial_states_;
        epsilonClosure(init_states);

        // create evaluation states
        std::list<std::pair<StateID, Word>> eval_states;
        for (const auto& s : init_states)
        {
            eval_states.push_back(std::make_pair(s, word));
        }

        // evaluate all states
        while (eval_states.size() > 0)
        {
            // extract first element from list
            StateID current_state = eval_states.front().first;
            Word current_word = eval_states.front().second;
            eval_states.pop_front();

            // if we are currently in an accepting state and we have nothing left to read, return true
            auto acc_find = state_accepting_.find(current_state);
            if (acc_find != state_accepting_.end())
            {
                if ((acc_find->second) and (current_word.size() == 0))
                {
                    return true;
                }
            }

            if (current_word.size() > 0)
            {
                // split word into first symbol and suffix
                Symbol current_symbol = current_word.front();
                Word suffix = current_word;
                suffix.pop_front();

                // calculate new enabled transitions
                auto trans_find = transitions_.find(std::make_pair(current_state, current_symbol));
                if (trans_find != transitions_.end())
                {
                    std::unordered_set<StateID> enabled_transitions = trans_find->second;

                    // create new states
                    for (const auto& new_state : enabled_transitions)
                    {
                        std::unordered_set<unsigned int> new_states = {new_state};
                        epsilonClosure(new_states);
                        for (auto s : new_states)
                        {
                            eval_states.emplace_back(std::make_pair(s, suffix));
                        }
                    }
                }
            }
        }
        return false;
    }

    /*
        ========================== store methods ==========================
    */

    // print FA to png
    void print_to_png(const std::string& filepath) const
    {
        int ret = 0;
        std::string s = "";

        // print to dot file
        print_to_file(filepath);

        // turn dot file to png
        s = utils::runCommand("dot -Tpng " + filepath + " -o " + filepath + ".png", ret);
    }

    // safe FA to dot file
    void print_to_file(const std::string& filepath) const
    {
        std::ofstream dotfile;
        dotfile.open(filepath);

        std::unordered_set<Symbol> symbols_with_epsilon = symbols_;
        symbols_with_epsilon.emplace(SILENT_SYMBOL);

        dotfile << "digraph G {" << std::endl;

        for (StateID i = 0; i < number_states_; i++)
        {
            StateName name = "undefined";
            bool initial = false;
            bool secret = false;
            bool accepting = false;

            auto name_find = state_name_.find(i);
            auto acc_find = state_accepting_.find(i);
            auto init_find = state_initial_.find(i);
            auto sec_find = state_secret_.find(i);
            if ((name_find != state_name_.end()) 
            and (acc_find != state_accepting_.end())
            and (init_find != state_initial_.end())
            and (sec_find != state_secret_.end()))
            {
                name = name_find->second;
                initial = init_find->second;
                secret = sec_find->second;
                accepting = acc_find->second;
            }
            else
            {
                throw std::runtime_error("undefined state index");
            }

            dotfile << "\"" << name << "\n" << i << "\"[";

            if (initial and accepting and secret)
            {
                dotfile << "fillcolor=gray50, style=filled, shape=box, peripheries=2";
            }
            else if (initial and accepting and not secret)
            {
                dotfile << "fillcolor=gray50, style=filled, peripheries=2";
            }
            else if (initial and not accepting and secret)
            {
                dotfile << "fillcolor=gray50, style=filled, shape=box";
            }
            else if (initial and not accepting and not secret)
            {
                dotfile << "fillcolor=gray50, style=filled";
            }
            else if (not initial and accepting and secret)
            {
                dotfile << "shape=box, peripheries=2";
            }
            else if (not initial and accepting and not secret)
            {
                dotfile << "peripheries=2";
            }
            else if (not initial and not accepting and secret)
            {
                dotfile << "shape=box";
            }
            else if (not initial and not accepting and not secret)
            {
                dotfile << "";
            }
            dotfile << "];" << std::endl;
        }

        for (StateID i = 0; i < number_states_; i++)
        {
            StateName origin_name = "undefined";
            auto origin_name_find = state_name_.find(i);
            if (origin_name_find != state_name_.end())
            {
                origin_name = origin_name_find->second;
            }
            else
            {
                throw std::runtime_error("undefined state index");
            }
            for (const auto& symbol_read : symbols_with_epsilon)
            {
                Symbol symbol = symbol_read;
                if (symbol_read == "tau")
                {
                    symbol = "τ";
                }
                else if (symbol_read == "epsilon")
                {
                    symbol = "ε";
                }

                auto trans_find = transitions_.find(std::make_pair(i, symbol_read));
                if (trans_find != transitions_.end())
                {
                    std::unordered_set<unsigned int> enabled_transitions = trans_find->second;
                    for (auto e : enabled_transitions)
                    {
                        std::string target_name = "undefined";
                        auto target_name_find = state_name_.find(e);
                        if (target_name_find != state_name_.end())
                        {
                            target_name = target_name_find->second;
                        }
                        else
                        {
                            throw std::runtime_error("undefined state index");
                        }
                        
                        dotfile << "\"" 
                            << origin_name << "\n" << i
                            << "\"->\"" 
                            << target_name << "\n" << e
                            << "\" [label=\"" 
                            << symbol
                            << "\"];" << std::endl;
                        
                    }
                }
            }
        }
        dotfile << "}" << std::endl;
        dotfile.close();
    }
};

#endif // FA_H_