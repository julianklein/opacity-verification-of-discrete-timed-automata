#ifndef GUARD_H_
#define GUARD_H_

#include <string.h>
#include <vector>

#include "interval_expression.h"
#include "utils.h"

/*
Contains the class Guard, representing guards of transitions in TA. The class provides a structure for Guards, as well
as functionality for the semantics.
*/

class Guard
{
private:
    std::vector<IntervalExpression> expressions_;

public:
    /*
        ========================== operator methods ==========================
    */
    friend std::ostream &operator<<(std::ostream &os, const Guard &guard)
    {
        os << guard.toString();
        return os;
    }

    bool operator==(const Guard &guard) const
    {
        return (guard.getExpressions() == expressions_);
    }

    bool operator!=(const Guard &guard) const
    {
        return not(guard == *this);
    }

    /*
        ========================== constructors ==========================
    */

    Guard() = default;
    
    Guard(const std::vector<IntervalExpression> expressions)
    {
        expressions_ = expressions;
    }

    /*
        ========================== get methods ==========================
    */

    const std::vector<IntervalExpression>& getExpressions() const
    {
        return expressions_;
    }

    /*
        ========================== evaluation methods ==========================
    */

    // print functions
    std::string toString() const
    {
        return utils::containerToString(expressions_);
    }

    // misc functions
    bool isEnabledBy(const ClockValuation& valuation) const
    {
        bool checker = true;

        int index = 0;
        for (auto ex : expressions_)
        {
            if (not ex.isIn(valuation[index]))
            {
                checker = false;
            }

            index = index + 1;
        }
        return checker;
    }
};

#endif // GUARD_H_