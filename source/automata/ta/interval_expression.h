#ifndef INTERVAL_EXPRESSION_H_
#define INTERVAL_EXPRESSION_H_

#include "utils.h"

/*
Contains the class IntervalExpression, representing an interval expression. An interval expression is a conjunction
of intervals that represent a set of guards on a specific clock. Each time instance contained in any intervals of the
conjunction enables the guard for the clock corresponding clock. The main idea of this class is to maintain the
member variable timeline_, which efficiently maintains the intervals.
*/

using IntervalSide = std::pair<TimeStamp, bool>;
using TimeLine = std::vector<IntervalSide>;

class IntervalExpression
{

private:
    TimeLine timeline_;

public:
    /*
        ========================== operator methods ==========================
    */
    friend std::ostream& operator<<(std::ostream& os, const IntervalExpression& intervalexpression)
    {
        os << intervalexpression.toString();
        return os;
    }

    bool operator==(const IntervalExpression& intervalexpression) const
    {
        IntervalExpression ie1 = IntervalExpression(timeline_);
        IntervalExpression ie2 = intervalexpression;

        ie1.clean();
        ie2.clean();

        return ie1.getTimeline() == ie2.getTimeline();
    }

    bool operator!=(const IntervalExpression &intervalexpression) const
    {
        return not(intervalexpression == *this);
    }

    /*
        ========================== constructors ==========================
    */

    IntervalExpression() = default;

    IntervalExpression(TimeLine timeline)
    {
        timeline_ = timeline;

        clean();
    }

    /*
        ========================== get methods ==========================
    */

    const TimeLine& getTimeline() const
    {
        return timeline_;
    }

    /*
        ========================== member methods ==========================
    */

    // removes unnecessary interval sides and sorts time line 
    void clean()
    {
        sort();
        TimeLine result;
        int opencount = 0;
        for (const auto& i : timeline_)
        {
            if (i.second)
            {
                if (opencount == 0)
                {
                    // edge case 3],[4 or 4],[4
                    if ((result.size() > 0) and ((result[result.size() - 1].first == i.first - 1) or (result[result.size() - 1].first == i.first)))
                    {
                        result.pop_back();
                    }
                    else
                    {
                        result.push_back(i);
                    }
                }
                opencount++;
            }
            else
            {
                opencount--;
                if (opencount == 0)
                {
                    result.push_back(i);
                }
            }
        }
        if ((result.size() > 0) and (result[result.size() - 1].first == INTERVAL_INFINITY))
        {
            result.pop_back();
        }
        timeline_ = result;
    }

    // sorts the interval sides in the time line
    void sort()
    {
        std::sort(timeline_.begin(), timeline_.end(), [] (const IntervalSide& a, const IntervalSide& b) -> bool 
        {
            if (a.first == b.first)
            {
                return a.second > b.second;
            }
            return a.first < b.first;
        });
    }

    // computes and returns a list of intervals from the time line, representing the interval expression
    std::list<Interval> getIntervals() const
    {
        std::list<Interval> result;
        if (timeline_.size() == 0)
        {
            return result;
        }

        auto iterator = timeline_.begin();
        while (iterator != timeline_.end())
        {
            IntervalSide left = *iterator;
            IntervalSide right;

            iterator++;
            if (iterator != timeline_.end())
            {
                right = *iterator;
                iterator++;
            }
            else
            {
                right = {INTERVAL_INFINITY, false};
            }

            result.push_back(std::make_pair(left.first, right.first));
        }
        return result;
    }

    // computes and returns the largest constant in the time line (below INTERVAL_INFINITY)
    unsigned int getMaxConstant() const
    {
        unsigned int result = 0;
        for (const auto& i : timeline_)
        {
            if (i.first > result)
            {
                result = i.first;
            }
        }
        return result;
    }

    // returns true if the interval expression is not closed by an interval side
    bool isOpen() const
    {
        return timeline_[timeline_.size() - 1].second;
    }

    // returns true if a given time stamp is inside an interval of the time line
    bool isIn(TimeStamp value) const
    {
        if (timeline_.size() == 0)
        {
            return false;
        }
        if (value < timeline_.begin()->first)
        {
            return false;
        }
        auto last = *timeline_.begin(); 
        for (const auto& i : timeline_)
        {
            if (i.first == value)
            {
                return true;
            }
            if (i.first > value)
            {
                return last.second;
            }
            last = i;
        }
        return last.second;
    }

    // returns string representation of the interval expression
    std::string toString() const
    {
        std::string result = "{";
        unsigned int j = 0;
        for (const auto& i : timeline_)
        {
            if (i.second)
            {
                result = result + "[" + std::to_string(i.first);
            }
            else
            {
                result = result + std::to_string(i.first) + "]";
            }
            if (j < timeline_.size() - 1)
            {
                result = result + ", ";
            }
            j++;
        }
        return result + "}";
    }
};

#endif // INTERVAL_EXPRESSION_H_