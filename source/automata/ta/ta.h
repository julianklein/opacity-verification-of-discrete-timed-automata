#ifndef TA_H_
#define TA_H_

#include <fstream>
#include <map>
#include <unordered_map>

#include "utils.h"
#include "guard.h"

/*
Contains the class TA, representing timed automata (TA). The class provides a structure for TA, as well as functionality
for the semantics (for reading input words) and storing to files (dot-files and png-files). For better readability, there
also exist custom type declarations.
*/

// TA specific defines
// state (location + clock valuation)
using SystemState = std::pair<StateID, ClockValuation>;
// Transition in a TA
using TATransition = std::tuple<Guard, ResetSet, StateID>;

// hash functions (unoptimized)
namespace std
{
    template <>
    struct hash<TATransition>
    {
        size_t operator()(const TATransition& x) const
        {
            return hash<std::string>()(std::get<0>(x).toString() + ", " + utils::containerToString(std::get<1>(x)) + ", " + std::to_string(std::get<2>(x)));
        }
    };

    template <>
    struct hash<SystemState>
    {
        size_t operator()(const SystemState& x) const
        {
            return hash<std::string>()(utils::clockVectorToString(std::get<1>(x)));
        }
    };
}

class TA
{

private:
    unsigned int number_states_;
    unsigned int number_clocks_;

    std::unordered_set<StateID> initial_states_;
    std::unordered_set<Symbol> symbols_;

    // states
    std::unordered_map<StateID, bool> state_accepting_;
    std::unordered_map<StateID, bool> state_secret_;
    std::unordered_map<StateID, bool> state_initial_;
    std::unordered_map<StateID, StateName> state_name_;

    std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<TATransition>> transitions_;
public:

    // empty constructor
    TA() = default;

    // stanadrd constructor
    TA(
        unsigned int number_states,
        unsigned int number_clocks,
        std::unordered_set<StateID> initial_states,
        std::unordered_set<Symbol> symbols,
        std::unordered_map<StateID, bool> state_accepting,
        std::unordered_map<StateID, bool> state_secret,
        std::unordered_map<StateID, bool> state_initial,
        std::unordered_map<StateID, StateName> state_name,
        std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<TATransition>> transitions
    )
    {
        number_states_ = number_states;
        number_clocks_ = number_clocks;
        initial_states_ = initial_states;
        symbols_ = symbols;
        state_accepting_ = state_accepting;
        state_secret_ = state_secret;
        state_initial_ = state_initial;
        state_name_ = state_name;
        transitions_ = transitions;
    }

    /*
        ========================== get methods ==========================
    */

    const unsigned int& get_number_states() const
    {
        return number_states_;
    }

    const unsigned int& get_number_clocks() const
    {
        return number_clocks_;
    }

    const std::unordered_set<StateID>& get_initial_states() const
    {
        return initial_states_;   
    }

    const std::unordered_set<Symbol>& get_symbols() const
    {   
        return symbols_;
    }

    const std::unordered_map<StateID, bool>& get_state_accepting() const
    {
        return state_accepting_;   
    }

    const std::unordered_map<StateID, bool>& get_state_secret() const
    {
        return state_secret_;   
    }

    const std::unordered_map<StateID, bool>& get_state_initial() const
    {
        return state_initial_;   
    }

    const std::unordered_map<StateID, StateName>& get_state_name() const
    {
        return state_name_;   
    }

    const std::unordered_map<std::pair<StateID, Symbol>, std::unordered_set<TATransition>>& get_transitions() const
    {
        return transitions_;   
    }

    // return all transition of a state
    std::unordered_set<TATransition> get_transitions_of_state(const StateID& state) const
    {
        std::unordered_set<TATransition> result;
        std::unordered_set<std::string> symbols_with_epsilon = symbols_;
        symbols_with_epsilon.emplace(SILENT_SYMBOL);
        for (const auto& symbol : symbols_with_epsilon)
        {
            auto trans_find = transitions_.find(std::make_pair(state, symbol));
            if (trans_find != transitions_.end())
            {
                std::unordered_set<TATransition> enabled = trans_find->second;
                for (const auto& e : enabled)
                {
                    result.emplace(e);
                }
            }
            
        }
        return result;
    }

    // returns all enabled transitions at state for symbol at time
    const std::unordered_set<TATransition> getEnabledTransitions(const SystemState& systemstate, const Symbol& symbol) const
    {
        std::unordered_set<TATransition> enabled_transitions;

        const StateID state = systemstate.first;
        const ClockValuation clocks = systemstate.second;

        auto transitions_find = transitions_.find(std::make_pair(state, symbol));
        if (transitions_find != transitions_.end())
        {
            std::unordered_set<TATransition> transitions = transitions_find->second;   
            
            for (const auto& t : transitions)
            {
                Guard guard = std::get<0>(t);
                if (guard.isEnabledBy(clocks))
                {
                    enabled_transitions.emplace(t);
                }            
            }
        }
        return enabled_transitions;
    }

    /*
        ========================== evaluation methods ==========================
    */

    // advance time of a vector by a time value
    ClockValuation advanceTime(const ClockValuation& vector, const unsigned int& value) const 
    {
        ClockValuation result (vector.size(), 0);
        for (unsigned int i = 0; i < result.size(); i++)
        {
            result[i] = vector[i] + value;
        }
        return result;
    }

    // apply resets to a system state 
    SystemState applyResets(const SystemState current, const ResetSet& resets) const
    {
        ClockValuation vec (number_clocks_, 0);
        SystemState result = std::make_pair(current.first, vec);
        for (unsigned int i = 0; i < current.second.size(); i++)
        {
            if (resets.find(i) == resets.end())
            {
                result.second[i] = current.second[i];
            }
        }
        return result;
    }

    // return true if the input word is accepted
    bool evaluate(TimedWord word) const
    {
        if (number_states_ == 0)
        {
            return false;
        }

        std::list<std::pair<SystemState, TimedWord>> eval_states;

        // create initital state and list of states to evaluate
        for (const auto& s : initial_states_)
        {
            ClockValuation vec (number_clocks_, 0);
            SystemState init_systemstate = std::make_pair(s, vec);
            eval_states.push_back(std::make_pair(init_systemstate, word));
        }

        while (eval_states.size() > 0)
        {
            // extract first element from list
            SystemState current_state = eval_states.front().first;
            TimedWord current_word = eval_states.front().second;
            eval_states.pop_front();

            // if we are currently in an accepting state and we have nothing left to read, return true
            auto acc_find = state_accepting_.find(current_state.first);
            if (acc_find != state_accepting_.end())
            {
                if ((acc_find->second) and (current_word.size() == 0))
                {
                    return true;
                }
            }

            if (current_word.size() > 0)
            {
                // split word into first event and suffix
                std::pair<Symbol, TimeStamp> current_symbol = current_word.front();
                TimedWord suffix = current_word;
                suffix.pop_front();

                // advance time in system state
                SystemState advanced_state = std::make_pair(
                    current_state.first, 
                    advanceTime(current_state.second, current_symbol.second)
                );

                const std::unordered_set<TATransition> enabled_transitions = getEnabledTransitions(
                    advanced_state,
                    current_symbol.first
                );

                // create new states
                for (auto transition : enabled_transitions)
                {
                    SystemState new_state = applyResets(std::make_pair(std::get<2>(transition), advanced_state.second), std::get<1>(transition));
                    eval_states.emplace_back(std::make_pair(new_state, suffix));
                }
            }
        }
        return false;
    }

    /*
        ========================== store methods ==========================
    */

    // print FA to png
    void print_to_png(const std::string& filepath) const
    {
        int ret = 0;
        std::string s = "";

        // print to dot file
        print_to_file(filepath);

        // turn dot file to png
        s = utils::runCommand("dot -Tpng " + filepath + " -o" + filepath + ".png", ret);
    }

    // safe TA to dot file
    void print_to_file(const std::string& filepath) const
    {
        std::ofstream dotfile;
        dotfile.open(filepath);

        dotfile << "digraph G {" << std::endl;

        for (StateID i = 0; i < number_states_; i++)
        {
            StateName name = "undefined";
            bool initial = false;
            bool secret = false;
            bool accepting = false;

            auto name_find = state_name_.find(i);
            auto acc_find = state_accepting_.find(i);
            auto init_find = state_initial_.find(i);
            auto sec_find = state_secret_.find(i);
            if ((name_find != state_name_.end()) 
            and (acc_find != state_accepting_.end())
            and (init_find != state_initial_.end())
            and (sec_find != state_secret_.end()))
            {
                name = name_find->second;
                initial = init_find->second;
                secret = sec_find->second;
                accepting = acc_find->second;
            }
            else
            {
                throw std::runtime_error("undefined state index");
            }

            dotfile << "\"" << name << "\n" << i << "\"[";

            if (initial and accepting and secret)
            {
                dotfile << "fillcolor=gray50, style=filled, shape=box, peripheries=2";
            }
            else if (initial and accepting and not secret)
            {
                dotfile << "fillcolor=gray50, style=filled, peripheries=2";
            }
            else if (initial and not accepting and secret)
            {
                dotfile << "fillcolor=gray50, style=filled, shape=box";
            }
            else if (initial and not accepting and not secret)
            {
                dotfile << "fillcolor=gray50, style=filled";
            }
            else if (not initial and accepting and secret)
            {
                dotfile << "shape=box, peripheries=2";
            }
            else if (not initial and accepting and not secret)
            {
                dotfile << "peripheries=2";
            }
            else if (not initial and not accepting and secret)
            {
                dotfile << "shape=box";
            }
            else if (not initial and not accepting and not secret)
            {
                dotfile << "";
            }
            dotfile << "];" << std::endl;
        }

        for (StateID i = 0; i < number_states_; i++)
        {
            StateName origin_name = "undefined";
            auto origin_name_find = state_name_.find(i);
            if (origin_name_find != state_name_.end())
            {
                origin_name = origin_name_find->second;
            }
            else
            {
                throw std::runtime_error("undefined state index");
            }

            std::unordered_set<Symbol> symbols_with_epsilon = symbols_;
            symbols_with_epsilon.emplace(SILENT_SYMBOL);
            for (const auto& symbol_read : symbols_with_epsilon)
            {
                Symbol symbol = symbol_read;
                if (symbol_read == "tau")
                {
                    symbol = "τ";
                }
                else if (symbol_read == "epsilon")
                {
                    symbol = "ε";
                }
                auto trans_find = transitions_.find(std::make_pair(i, symbol_read));
                if (trans_find != transitions_.end())
                {
                    std::unordered_set<TATransition> enabled_transitions = trans_find->second;
                    for (auto e : enabled_transitions)
                    {
                        Guard guard = std::get<0>(e);
                        std::unordered_set<unsigned int> reset_set = std::get<1>(e);
                        unsigned int target = std::get<2>(e);

                        std::string target_name = "undefined";
                        auto target_name_find = state_name_.find(target);
                        if (target_name_find != state_name_.end())
                        {
                            target_name = target_name_find->second;
                        }
                        else
                        {
                            throw std::runtime_error("undefined state index");
                        }
                        
                        dotfile << "\"" 
                            << origin_name << "\n" << i
                            << "\"->\"" 
                            << target_name << "\n" << target
                            << "\" [label=\"" 
                            << symbol << ", " << guard.toString() << ", " << utils::containerToString(reset_set)
                            << "\"];" << std::endl;
                        
                    }
                }
            }
        }
        dotfile << "}" << std::endl;
        dotfile.close();
    }
};


#endif // TA_H_