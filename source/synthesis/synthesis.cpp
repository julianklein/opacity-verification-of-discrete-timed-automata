#include "synthesis.h"

std::string generateNewSeed(const std::string& initialSeed) {
    // Static variables to retain state across function calls
    static std::string seed = initialSeed;
    static unsigned int counter = 0;

    // Generate a new seed by combining the current seed with a counter value
    std::ostringstream oss;
    oss << seed << "_" << counter++;

    return oss.str();
}

IntervalExpression get_random_interval_expression(
    const unsigned int& numberIntervals, 
    const unsigned int& minValue,
    const unsigned int& maxValue,
    const bool& enableInfinity,
    const std::string& seed
)
{
    std::vector<std::pair<unsigned int, bool>> timeline;
    for (unsigned int i = 0; i < numberIntervals; i++)
    {
        // get random interval
        unsigned int min = minValue + (std::rand() % (maxValue - minValue + 1));
        unsigned int max = minValue + (std::rand() % (maxValue - minValue + 1));


        std::hash<std::string> hasher;
        std::size_t numericalSeed = hasher(seed);

        // Create a random number generator engine with the numerical seed
        std::mt19937 gen(static_cast<unsigned int>(numericalSeed));
        std::uniform_real_distribution<double> distribution(minValue, maxValue);
        double randomValue = distribution(gen);
        

        min = randomValue;
        
        std::normal_distribution<double> ndistribution(min, 5);

        double val = ndistribution(gen);
        if (val < 0)
        {
            val = val * (-1);
        }

        unsigned int offset = std::round(val);

        max = min + offset;// + ndistribution(gen);

        if (min > max)
        {
            unsigned int swap = min;
            min = max;
            max = swap;
        }

        if (enableInfinity)
        {
            if (max == maxValue - 1)
            {
                max = INTERVAL_INFINITY;
            }
        }

        timeline.push_back({min, true});
        if (max != INTERVAL_INFINITY)
        {
            timeline.push_back({max, false});
        }
    }

    IntervalExpression ie = IntervalExpression(timeline);

    if (ie.getIntervals().size() == 0) // this should not happen
    {
        return IntervalExpression({{1, true}, {1, false}});
    }

    return ie;
}

std::string synthesis::get_random_seed(const unsigned int length)
{
    std::string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    int chars = characters.size();

    std::mt19937_64 rng(std::time(nullptr));
    std::uniform_int_distribution<int> distribution(0, chars - 1);

    std::string seed;
    for (unsigned int i = 0; i < length; ++i) 
    {
        int randomIndex = distribution(rng);
        seed += characters[randomIndex];
    }

    return seed;
}

TimedWord synthesis::getRandomWord(
    const std::unordered_set<Symbol> symbols,
    const unsigned int lowerLengthBound,
    const unsigned int upperLengthBound, 
    const unsigned int lowerTimeBound, 
    const unsigned int upperTimeBound
    )
{
    int number_symbols = symbols.size();
    std::map<int, Symbol> symbol_map;
    int index = 0;
    for (auto s : symbols)
    {
        symbol_map[index] = s;
        index = index + 1;
    }
    unsigned int length = lowerLengthBound + (std::rand() % (upperLengthBound - lowerLengthBound + 1));

    TimedWord result;

    for (decltype(length) l = 0; l < length; l++)
    {
        TimeStamp time = lowerTimeBound + (std::rand() % (upperTimeBound - lowerTimeBound + 1));
        int symbol_index = std::rand() % number_symbols;
        Symbol symbol = symbol_map[symbol_index];
        std::pair<Symbol, TimeStamp> e = std::make_pair(symbol, time);
        result.push_back(e);
    }
    return result;
}

TA synthesis::getRandomTA(
    const unsigned int number_clocks,
    const unsigned int number_states,
    const unsigned int number_initial_states,
    const unsigned int number_secret_states,
    const unsigned int number_accepting_states,
    const unsigned int number_symbols,
    const unsigned int number_observable_symbols,
    const double mean,
    const double deviation,
    const std::string seed,
    const unsigned int min_time,
    const unsigned int max_time,
    const unsigned int constraint_length,
    const bool use_silent_transitions=false
)
{
    // values for TA constructor
    unsigned int number_states_ = number_states;
    unsigned int number_clocks_ = number_clocks;
    std::unordered_set<unsigned int> initial_states_;
    std::unordered_set<std::string> symbols_;
    std::unordered_map<unsigned int, bool> state_accepting_;
    std::unordered_map<unsigned int, bool> state_secret_;
    std::unordered_map<unsigned int, bool> state_initial_;
    std::unordered_map<unsigned int, std::string> state_name_;
    std::unordered_map<std::pair<unsigned int, std::string>, std::unordered_set<TATransition>> transitions_;

    // use seed
    std::hash<std::string> hasher;
    auto hashed = hasher(seed);
    srand(hashed);

    // create symbols
    if (use_silent_transitions)
    {
        symbols_.emplace(SILENT_SYMBOL);
    }
    for (unsigned int i = 0; i < number_symbols; i++)
    {
        symbols_.emplace("s" + std::to_string(i));
    }

    // create state names
    for (StateID i = 0; i < number_states_; i++)
    {
        state_name_[i] = "x" + std::to_string(i);
        state_initial_[i] = false;
        state_secret_[i] = false;
        state_accepting_[i] = false;
    }

    // get index for every initial state
    std::set<StateID> initial_index;
    while (initial_index.size() < number_initial_states)
    {
        StateID s = std::rand() % number_states_;
        initial_index.emplace(s);
        state_initial_[s] = true;
        initial_states_.emplace(s);
    }

    // get index for every secret state
    std::set<StateID> secret_index;
    while (secret_index.size() < number_secret_states)
    {
        StateID s = std::rand() % number_states_;
        secret_index.emplace(s);
        state_secret_[s] = true;
    }

    // get index for every accepting state
    std::set<StateID> accepting_index;
    while (accepting_index.size() < number_accepting_states)
    {
        StateID s = std::rand() % number_states_;
        accepting_index.emplace(s);
        state_accepting_[s] = true;
    }

    // create transitions
    std::seed_seq seed1(seed.begin(), seed.end());
    std::default_random_engine generator(seed1);
    std::normal_distribution<double> distribution(mean, deviation);

    for (StateID i = 0; i < number_states; i++)
    {
        // decide how many transitions state i has

        double number = distribution(generator);
        int new_number = std::floor(number);
        if (new_number < 0)
        {
            new_number = 0;
        }
        unsigned int ntrans = (unsigned int) (new_number);
        
        for (unsigned int j = 0; j < ntrans; j++)
        {
            // get target state and symbol
            StateID to_index = std::rand() % number_states;

            unsigned int symbol_index = std::rand() % number_symbols;
            auto it_symbol = symbols_.begin();
            std::advance(it_symbol, symbol_index);
            Symbol symbol = *it_symbol;


            // generate reset set
            std::unordered_set<unsigned int> base;
            for (unsigned int j = 0; j < number_clocks; j++)
            {
                base.emplace(j);
            }
            std::unordered_set<std::unordered_set<unsigned int>> powerset = utils::unordered_setPowerset(base);
            unsigned int reset_index = std::rand() % powerset.size();
            auto it_reset_set = powerset.begin();
            std::advance(it_reset_set, reset_index);
            std::unordered_set<unsigned int> reset_set = *it_reset_set;

            // create random guard
            std::vector<IntervalExpression> expressions;
            for (unsigned int j = 0; j < number_clocks; j++)
            {
                IntervalExpression ie = get_random_interval_expression(constraint_length, min_time, max_time, false, generateNewSeed(seed));
                expressions.push_back(ie);
            }
            Guard guard = Guard(expressions);

            transitions_[std::make_pair(i, symbol)].emplace(std::make_tuple(guard, reset_set, to_index));
        }
    }
    return TA(
        number_states_,
        number_clocks_,
        initial_states_,
        symbols_,
        state_accepting_,
        state_secret_,
        state_initial_,
        state_name_,
        transitions_
    );
}

