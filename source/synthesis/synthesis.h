#include <random>
#include <ctime>

#include "ta.h"
#include "fa.h"

/*
Contains a name space with functionality to create randomized test words and automata for evaluation.
*/

namespace synthesis
{
    std::string get_random_seed(const unsigned int length);

    TimedWord getRandomWord(
        const std::unordered_set<Symbol> symbols,
        const unsigned int lowerLengthBound,
        const unsigned int upperLengthBound,
        const unsigned int lowerTimeBound,
        const unsigned int upperTimeBound
    );

    TA getRandomTA(
        const unsigned int number_clocks,
        const unsigned int number_states,
        const unsigned int number_initial_states,
        const unsigned int number_secret_states,
        const unsigned int number_accepting_states,
        const unsigned int number_symbols,
        const unsigned int number_observable_symbols,
        const double mean,
        const double deviation,
        const std::string seed,
        const unsigned int min_time,
        const unsigned int max_time,
        const unsigned int constraint_length,
        const bool use_silent_transitions
    );
}