#ifndef UTILS_H_
#define UTILS_H_

#include <string>
#include <algorithm>
#include <set>
#include <vector>
#include <array>
#include <cmath>
#include <unordered_set>
#include <sstream>
#include <iostream>
#include <list>

#include "constants.h"

/*
Contains a name space with miscellaneous functions.
*/

namespace utils
{
    std::string clockVectorToString(ClockValuation v);
    std::string timedWordToString(TimedWord v);
    std::string runCommand(const std::string cmd, int& out_exitStatus);


    template <typename containerT>
    std::string containerToString(const containerT& container, const bool multiline = false)
    {
        std::string ret = "{";
        decltype(container.size()) counter = 0;
        for (auto i : container)
        {
            std::stringstream ss;
            ss << i;
            ret = ret + ss.str();
            if (counter + 1 != container.size())
            {
                ret = ret + ", ";
                if (multiline)
                {
                    ret = ret + "\n";
                }
            }
            counter = counter + 1;
        }
        ret = ret + "}";
        return ret;
    }

    template <typename typeT>
    std::unordered_set<std::unordered_set<typeT>> unordered_setPowerset(std::unordered_set<typeT> s)
    {
        // set sizes
        unsigned int pow_set_size = std::pow(2, s.size());
        int set_size = s.size();

        // array of set values
        typeT a[s.size()];
        // resulting list of subsets
        std::unordered_set<std::unordered_set<typeT>> l;

        // fill array with set values
        int c = 0;
        for (auto i : s)
        {
            a[c] = i;
            c = c + 1;
        }

        // for every subset configuration
        for (decltype(pow_set_size) counter = 0; counter < pow_set_size; counter++)
        {
            // collector for subset entries
            std::unordered_set<typeT> s;
            // go through bit sequence of counter and insert if bit is 1
            for (int j = 0; j < set_size; j++)
            {
                if (counter & (1 << j))
                {
                    s.insert(a[j]);
                }
            }
            // add subset
            l.insert(s);
        }

        return l;
    }
}

namespace std
{
    template <>
    struct hash<std::unordered_set<unsigned int>>
    {
        size_t operator()(const std::unordered_set<unsigned int>& x) const
        {
            std::vector<unsigned int> l(x.begin(), x.end());
            std::sort(l.begin(), l.end());

            return hash<std::string>()(utils::clockVectorToString(l));
        }
    };

    template <>
    struct hash<std::pair<unsigned int, unsigned int>>
    {
        size_t operator()(const std::pair<unsigned int, unsigned int>& x) const
        {
            return hash<std::string>()(std::to_string(x.first) + ", " + std::to_string(x.second));
        }
    };

    template <>
    struct hash<std::tuple<unsigned int, unsigned int, unsigned int>>
    {
        size_t operator()(const std::tuple<unsigned int, unsigned int, unsigned int>& x) const
        {
            return hash<std::string>()(std::to_string(std::get<0>(x)) + ", " + std::to_string(std::get<1>(x)) + ", " + std::to_string(std::get<2>(x)));
        }
    };

    template <>
    struct hash<std::pair<StateID, Symbol>>
    {
        size_t operator()(const std::pair<StateID, Symbol>& x) const
        {
            return hash<std::string>()(std::to_string(x.first) + ", " + x.second);
        }
    };

    template <>
    struct hash<ClockValuation>
    {
        size_t operator()(const ClockValuation& x) const
        {
            return hash<std::string>()(utils::clockVectorToString(x));
        }
    };
}


#endif // UTILS