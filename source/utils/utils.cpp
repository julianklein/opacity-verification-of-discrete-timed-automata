#include "utils.h"

std::string utils::runCommand(const std::string cmd, int &out_exitStatus)
{
    out_exitStatus = 0;
    auto pPipe = ::popen(cmd.c_str(), "r");
    if (pPipe == nullptr)
    {
        throw std::runtime_error("Cannot open pipe");
    }
    std::array<char, 256> buffer;
    std::string result;
    while (not std::feof(pPipe))
    {
        auto bytes = std::fread(buffer.data(), 1, buffer.size(), pPipe);
        result.append(buffer.data(), bytes);
    }
    auto rc = ::pclose(pPipe);
    if (WIFEXITED(rc))
    {
        out_exitStatus = WEXITSTATUS(rc);
    }
    if (out_exitStatus != 0)
    {
        std::cerr << "Error running\n" << result << std::endl;
    }
    return result;
}

std::string utils::clockVectorToString(ClockValuation v)
{
    if (v.size() == 0)
    {
        return "<>";
    }
    std::string result = "<";
    for (decltype(v.size()) i = 0; i < v.size() - 1; i++)
    {
        result = result + std::to_string(v[i]) + ", ";
    }
    result = result + std::to_string(v[v.size() - 1]) + ">";
    return result;
}

std::string utils::timedWordToString(const TimedWord v)
{
    std::string result = "";
    for (auto t : v)
    {
        result = result + "(" + std::get<0>(t) + ", " + std::to_string(std::get<1>(t)) + ")";
    }
    return result;
}