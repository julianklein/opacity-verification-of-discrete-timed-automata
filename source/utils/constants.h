#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#include <unordered_set>
#include <map>

// id of a state
using StateID = unsigned int;
// id of a clock
using ClockID = unsigned int;
// name of a state
using StateName = std::string;
// input symbol 
using Symbol = std::string;
// time stamp of some event
using TimeStamp = unsigned int;
// timed input symbol 
using Event = std::pair<Symbol, TimeStamp>;
// clock valuation for a given set of clocks
using ClockValuation = std::vector<TimeStamp>;
// set of clocks to be reset on a transition
using ResetSet = std::unordered_set<ClockID>;

// word defines
using Word = std::list<Symbol>;
using TimedWord = std::list<Event>;

// interval (inclusive)
using Interval = std::pair<unsigned int, unsigned int>;

// state dependent abstraction
using Abstraction = std::map<StateID, ClockValuation>;

// constant values
#define INTERVAL_INFINITY 99999
#define SILENT_SYMBOL "epsilon"
#define TICK_SYMBOL "tau"
#define INPUT_PATH "input"
#define OUTPUT_PATH "output"

#endif // CONSTANTS_H_