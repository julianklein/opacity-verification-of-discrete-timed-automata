Copyright (C) 2024, Julian Klein

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.


# Transforming Timed Automata to Tick Automata to verify Opacity
In this git repository, we provide our prototype implementation of our novel 
transformation from *timed automata* (TA) to *tick automata* (𝜏-FA) in a discrete time setting.
We used this tool in the evaluation in our paper that can be found here: [https://doi.org/10.1145/3644033.3644376](https://doi.org/10.1145/3644033.3644376)
Furthermore, we provide the full evaluation setup (including our case studies).
A full documentation of all provided code can be found in `documentation.pdf`.

## Dependencies
Our evaluation tool is written in C++.
Our plotting tool is written in python.
It is therefore required to install `python3`, `make`, and a C++ compiler (we use `g++`).
On linux systems, we therefore recommend to install `python3` and `build-essential`.
Furthermore, we use LaTeX to generate plots with our plotting tool.
On linux systems, we therefore recommend to install `texlive-full`.

## Evaluation Tool
We provide a **Makefile** in the base directory.

To run our evaluation, use
```
make evaluator
./evaluator
```
To print the case studies to files, use
```
make print
./printer
```

## Plotting Tool
All produced files can be found in `output`.
We provide python scripts to produce plots from the produced .csv files.
The python scripts can be found in `evaluation/plotting`.

After running the evaluator, all plots can be generated using `full_plotter.py`.
To immediatly open the plots, use the `open` option.

So to replicate our results from the paper, run:
```
make evaluator
./evaluator
cd evaluation/plotting
python3 full_plotter.py open
```

## Case Studies
| Case Study      | File                           |Source                                                                  |
| ----------------|:------------------------------:|------------------------------------------------------------------------|
| Web Shop        | `input/case_studies/grid.json` | https://doi.org/10.1016/j.jisa.2021.102926                             |
| Medical Service | `input/case_studies/grid.json` | https://doi.org/10.1109/SOCA.2015.33                                   |
| Sensor Network  | `input/case_studies/grid.json` | Constructed by ourself, specification in `input/case_studies/grid.pdf` |
| ATM             | `input/case_studies/grid.json` | https://doi.org/10.1145/3563822.3568013                                |
